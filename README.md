# Emča & Ondra Tourist Diary

I promise myself that after the corona pandemic ends, I will visit with my girlfriend Emča so many places around the Czech Republic. This service as part of NNPIA semestral work should help us to track all of them.

## Application workflow
For security reason si application available only for registered users.

### View all interesting places 🌄

And search that you are interesting in.

![Import tourt card](resources/images/select_all.png)
![Import tourt card](resources/images/search.png)

### Take a detail look on every place 👀

Its including map and other useful information. And you can check place when you visit it.

![Import tourt card](resources/images/detail.png)

### Save memories for later generations. ❤️

Our application is capable of storing photos. So you can sort then on one place.

![Import tourt card](resources/images/memories.png)


### Need more? 🤔

On main page you dont find any description but always some tips to places that you didnt visit yet.

![Import tourt card](resources/images/tips.png)

### Admin

The default Administrator account with login credentials below is created after the first
start of backend service.

*User*: **root**

*Password*: **heslo**

Administrator have a little more privileges than a basic user. For example:

### Import tour card ⬇️

You can create your application based on public database of Western Tour cards. Just
import the excel file and backend service will do the rest.

***Application wont let you to import schema if there is already some data!***

![Import tourt card](resources/images/import.png)

### Create new user 🔐

If you want to join your friends, just create a basic account.

![Import tourt card](resources/images/create_user.png)

### Database schema

![Database schema](resources/database/tourist_diary.png)

### Backend

* Kotlin
* Java
* Gradle
* Spring boot
* Spring security
* MySQL database for production data (migrated to PostgreSQL on production)
### Frontend
* ReactJS
* React Bootstrap

## Installation

To run this service:

1. Clone project repository to your server
2. Open project folder
3. Modify ***src/main/resources/application.properties*** for database credentials.

## Run

### Backend and Frontend

```
gradle bootRun
```

### Only Frontend

```
npm run start
```

All dependencies and libraries will download automatically

## Project structure
* Folder ***src/main/java*** contains backend
* Folder ***src/main/webapp*** contains frontend
* Folder ***src/main/resources*** contains backend configuration

## Tests ⚠️✅

### Selenium (UI tests)

* [Success/Failed login](https://gitlab.com/st55774/nnpia-semestralni-prace/-/blob/master/src/test/kotlin/cz/upce/fei/travelcards/ui/AuthenticationTest.kt)
* [Insert into form](https://gitlab.com/st55774/nnpia-semestralni-prace/-/blob/master/src/test/kotlin/cz/upce/fei/travelcards/ui/InterestPointTest.kt)

### Integrate test

* [Tips to trip is empty when user add card to collection](https://gitlab.com/st55774/nnpia-semestralni-prace/-/blob/master/src/test/kotlin/cz/upce/fei/travelcards/service/tourcard/TourCardDashboardServiceTest.kt)

### Basic tests

* [The tour cards extractor](https://gitlab.com/st55774/nnpia-semestralni-prace/-/tree/master/src/test/kotlin/cz/upce/fei/travelcards/service/excel/extractor)