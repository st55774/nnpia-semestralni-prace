package cz.upce.fei.travelcards.controller.auth

import com.fasterxml.jackson.databind.ObjectMapper
import cz.upce.fei.travelcards.message.request.LoginForm
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc

import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status


@SpringBootTest
@AutoConfigureMockMvc
internal class AuthenticationControllerTest {
    @Autowired private lateinit var mvc : MockMvc
    @Autowired private lateinit var objectMapper: ObjectMapper

    @Test
    fun loginSuccess() {
        val loginForm = LoginForm(username = USERNAME, password = PASSWORD)
        val result = mvc.perform(post(API_PATH)
            .content(objectMapper.writeValueAsString(loginForm))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk()).andReturn()
        val response = result.response.contentAsString
        assertTrue(response.contains("accessToken2"))
    }

    @Test
    fun loginNotSuccess() {
        val loginForm = LoginForm(username = USERNAME_NOT_EXIST, password = PASSWORD)
        val result = mvc.perform(post(API_PATH)
            .content(objectMapper.writeValueAsString(loginForm))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().is4xxClientError).andReturn()
        val response = result.response.contentAsString
        assertFalse(response.contains("accessToken2"))
    }

    private companion object{
        const val API_PATH = "/api/auth/signin"
        const val USERNAME = "root"
        const val USERNAME_NOT_EXIST = "notRoot"
        const val PASSWORD = "heslo"
    }
}