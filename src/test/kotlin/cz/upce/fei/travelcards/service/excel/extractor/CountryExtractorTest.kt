package cz.upce.fei.travelcards.service.excel.extractor

import cz.upce.fei.travelcards.service.excel.extractor.CountryExtractor
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
internal class CountryExtractorTest {
    @Autowired
    private lateinit var countryExtractor : CountryExtractor

    @Test
    fun readInterestPointsCount() =
        assertEquals(5, countryExtractor.readAll(PATH).count())

    @Test
    fun readInterestPoints() =
        assertTrue(countryExtractor
            .readAll(PATH)
            .map { it.idShort }
        .containsAll(setOf("BDv", "Jh", "JKS", "ZČ", "ZSBK")))

    @Test
    fun onlyAllowedInclude() =
        assertFalse(countryExtractor.readAll(PATH).map{ it.name }.contains("ALD"))

    private companion object{
        const val PATH = "src/test/resources/TCards.xlsx"
    }
}