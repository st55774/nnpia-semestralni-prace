package cz.upce.fei.travelcards.service.tourcard

import cz.upce.fei.travelcards.creator.Creator
import cz.upce.fei.travelcards.dto.TourCardDto
import cz.upce.fei.travelcards.entity.*
import cz.upce.fei.travelcards.entity.role.RoleName
import cz.upce.fei.travelcards.repository.TourCardRepository
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Import

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.AUTO_CONFIGURED)
@Import(Creator::class)
internal class TourCardDashboardServiceTest {
    @Autowired
    private lateinit var creator: Creator
    @Autowired
    private lateinit var tourCardRepository : TourCardRepository
    @Autowired
    private lateinit var tourCardDashboardService: TourCardDashboardService
    @Autowired
    private lateinit var tourCardUserService: TourCardUserService

    private val user = User(username = USERNAME, password = PASSWORD, email = EMAIL)

    @BeforeEach
    fun setUp() {
        creator.saveEntities(user)
        creator.saveEntities(tourCard)
    }

    @Test
    fun getDashboard() {
        val tourCard = tourCardRepository.findAll()
        tourCardUserService.add(TourCardDto(id = tourCard.first().id), user)
        assertTrue(tourCardDashboardService.getDashboard(user).tips.isEmpty())
    }

    private companion object{
        const val USERNAME = "root2"
        const val PASSWORD = "heslo"
        const val EMAIL ="root@root.cz"

        val tourCard = TourCard(title = "testt", coordinate = "testt")
    }
}