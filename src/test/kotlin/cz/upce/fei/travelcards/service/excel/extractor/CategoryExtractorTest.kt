package cz.upce.fei.travelcards.service.excel.extractor

import cz.upce.fei.travelcards.entity.InterestPoint
import cz.upce.fei.travelcards.repository.InterestPointRepository
import cz.upce.fei.travelcards.service.excel.extractor.CategoryExtractor
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.AUTO_CONFIGURED)
internal class CategoryExtractorTest{
    @Autowired
    private lateinit var categoryExtractor : CategoryExtractor
    @Autowired
    private lateinit var interestPointRepository: InterestPointRepository

    @BeforeEach
    fun beforeEach() =
        arrayOf("TURISTIKA", "KULTURA", "GASTRONOMIE", "SPORT")
            .forEach { interestPointRepository.save(InterestPoint(name = it)) }

    @Test
    fun readCategories() =

        assertTrue(categoryExtractor
            .readAll(PATH)
            .map { it.sid }
            .containsAll(setOf("CA", "CVZ", "GVN", "N-S")))

    private companion object{
        const val PATH = "src/test/resources/TCards.xlsx"
    }
}