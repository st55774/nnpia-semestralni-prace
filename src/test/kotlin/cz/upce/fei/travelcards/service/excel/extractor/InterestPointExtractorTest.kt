package cz.upce.fei.travelcards.service.excel.extractor

import cz.upce.fei.travelcards.service.excel.extractor.InterestPointExtractor
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
internal class InterestPointExtractorTest{
    @Autowired
    private lateinit var interestPointExtractor : InterestPointExtractor

    @Test
    fun readInterestPointsCount() =
        assertEquals(4, interestPointExtractor
            .readAll(PATH)
            .count())

    @Test
    fun readInterestPoints() =
        assertTrue(interestPointExtractor
            .readAll(PATH)
            .map { it.name }
            .containsAll(setOf("TURISTIKA", "KULTURA", "GASTRONOMIE", "SPORT"))
        )

    private companion object{
        const val PATH = "src/test/resources/TCards.xlsx"
    }
}