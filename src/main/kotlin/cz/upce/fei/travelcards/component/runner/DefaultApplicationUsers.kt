package cz.upce.fei.travelcards.component.runner

import cz.upce.fei.travelcards.entity.Role
import cz.upce.fei.travelcards.entity.User
import cz.upce.fei.travelcards.entity.role.RoleName
import cz.upce.fei.travelcards.repository.RoleRepository
import cz.upce.fei.travelcards.repository.UserRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Bean
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component

@Component
class DefaultApplicationUsers(
    private val roleRepository: RoleRepository,
    private val userRepository: UserRepository,
    private val encoder : PasswordEncoder
) {
    @Bean
    fun addDefaultUser() : CommandLineRunner{
        return CommandLineRunner {
            mutableListOf(RoleName.ROLE_USER, RoleName.ROLE_ADMIN)
                .forEach {
                    if(!roleRepository.existsByRoleName(it))
                        roleRepository.save(Role(roleName = it))
                }

            val root = userRepository.findByUsername(ROOT_NAME)
            if(!root.isPresent()){
                userRepository.save(User(
                    username = ROOT_NAME,
                    email = ROOT_EMAIL,
                    password = encoder.encode(ROOT_PASSWORD),
                    roles = mutableSetOf(
                        roleRepository.findByRoleName(RoleName.ROLE_USER).get(),
                        roleRepository.findByRoleName(RoleName.ROLE_ADMIN).get()
                    )
                ))
            }
        }
    }

    private companion object{
        const val ROOT_NAME = "root"
        const val ROOT_PASSWORD = "heslo"
        const val ROOT_EMAIL = "ondrej.chrbolka@gmail.com"
    }
}