package cz.upce.fei.travelcards.component

import cz.upce.fei.travelcards.entity.Category
import cz.upce.fei.travelcards.entity.Country
import cz.upce.fei.travelcards.entity.TourCard
import cz.upce.fei.travelcards.entity.User

import org.springframework.data.rest.core.config.RepositoryRestConfiguration
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer
import org.springframework.stereotype.Component

@Component
class ExposeEntityIdRestMvcConfiguration : RepositoryRestConfigurer {
    override fun configureRepositoryRestConfiguration(config: RepositoryRestConfiguration) {
        config.exposeIdsFor(Country::class.java)
        config.exposeIdsFor(Category::class.java)
        config.exposeIdsFor(TourCard::class.java)
        config.exposeIdsFor(User::class.java)
    }
}