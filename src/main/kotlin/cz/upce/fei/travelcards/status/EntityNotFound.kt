package cz.upce.fei.travelcards.status

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.BAD_REQUEST)
class  EntityNotFound(message : String = "Entity not found.") : Throwable(message = message)