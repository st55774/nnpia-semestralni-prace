package cz.upce.fei.travelcards.status.tourcard

import cz.upce.fei.travelcards.entity.TourCard
import cz.upce.fei.travelcards.entity.User
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.BAD_REQUEST)
class TourCardNotIncluded(tourCard: TourCard, user: User)
    : Throwable("Tour card \"${tourCard.title}\" is not included in ${user.username} collection.") {
}