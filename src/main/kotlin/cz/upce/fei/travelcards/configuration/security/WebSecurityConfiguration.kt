package cz.upce.fei.travelcards.configuration.security

import cz.upce.fei.travelcards.component.jwt.JwtAuthEntryPoint
import cz.upce.fei.travelcards.component.jwt.JwtAuthTokenFilter

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class WebSecurityConfiguration(
    private val userDetailsService : UserDetailsService,
    private val unauthorizedHandler : JwtAuthEntryPoint,
    private val jwtAuthTokenFilter : JwtAuthTokenFilter
) : WebSecurityConfigurerAdapter() {
    @Bean
    fun authenticationJwtTokenFilter() = jwtAuthTokenFilter

    @Throws(Exception::class)
    public override fun configure(authenticationManagerBuilder: AuthenticationManagerBuilder) {
        authenticationManagerBuilder
            .userDetailsService(userDetailsService)
            .passwordEncoder(passwordEncoder())
    }

    @Bean
    @Throws(Exception::class)
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http.cors().and().csrf().disable().authorizeRequests()
            .antMatchers(Permited.API, Permited.ROOT, Permited.STATIC, Permited.IMAGES, Permited.REGISTER,
                Permited.LOGIN, Permited.HOME, Permited.MANIFEST, Permited.FAVICON, Permited.LOGO_192,
                Permited.LOGO_512, Permited.ROBOTS).permitAll()
            .anyRequest().authenticated()
            .and()
            .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter::class.java)
    }

    private object Permited{
        const val API = "/api/auth/**"
        const val ROOT = "/"
        const val STATIC = "/static/**"
        const val REGISTER = "/signup"
        const val LOGIN = "/signin"
        const val HOME = "/home"
        const val IMAGES = "/images/**"
        const val MANIFEST = "/manifest.json"
        const val FAVICON = "/favicon.ico"
        const val LOGO_192 = "/logo192.png"
        const val LOGO_512 = "/logo512.png"
        const val ROBOTS = "/robots.txt"
    }
}