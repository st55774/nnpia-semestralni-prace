package cz.upce.fei.travelcards.dto

data class InterestPointDto(
    var id : Long = 0L,
    var name : String = ""
)
