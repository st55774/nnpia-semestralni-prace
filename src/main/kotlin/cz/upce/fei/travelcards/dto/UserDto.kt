package cz.upce.fei.travelcards.dto

import cz.upce.fei.travelcards.entity.Role
import cz.upce.fei.travelcards.entity.TourCard
import java.util.*

data class UserDto(
    var id : Long = 0,
    var username : String = "",
    var password : String = "",
    var create_time : Date = Date(),
    var surname : String = "",
    var lastname : String = "",
    var email : String = "",
    var description : String = "",
    var active : Boolean = true,
    var tourCards: Set<TourCard> = mutableSetOf(),
    var roles : Set<Role> = mutableSetOf()
)