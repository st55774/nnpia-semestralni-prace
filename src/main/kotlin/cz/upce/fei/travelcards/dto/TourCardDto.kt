package cz.upce.fei.travelcards.dto

import cz.upce.fei.travelcards.entity.Category
import cz.upce.fei.travelcards.entity.Country
import cz.upce.fei.travelcards.entity.Photo
import cz.upce.fei.travelcards.entity.User

data class TourCardDto(
    var id : Long = 0,
    var title : String = "",
    var subtitle : String = "",
    var coordinate : String = "",
    var web : String = "",
    var active : Boolean = true,
    var photo : Photo? = null,
    var users : MutableSet<User> = mutableSetOf(),
    var category : Category = Category(),
    var country : Country = Country()
)