package cz.upce.fei.travelcards.dto

import cz.upce.fei.travelcards.entity.role.RoleName

data class RoleDto(
    var id : Long = 0,
    var roleName : RoleName = RoleName.ROLE_USER
)