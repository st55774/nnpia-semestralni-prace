package cz.upce.fei.travelcards.dto

data class CountryDto(
    var id : Long = 0L,
    var idShort : String = "",
    var name : String = "",
    var state : String = "",
)