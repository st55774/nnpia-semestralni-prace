package cz.upce.fei.travelcards.dto

import cz.upce.fei.travelcards.entity.InterestPoint

data class CategoryDto(
    var id : Long = 0L,
    var sid : String = "",
    var title : String = "",
    var interestPoint: InterestPoint = InterestPoint()
)