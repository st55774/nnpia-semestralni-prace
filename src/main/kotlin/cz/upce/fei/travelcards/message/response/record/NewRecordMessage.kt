package cz.upce.fei.travelcards.message.response.record

data class NewRecordMessage<T>(val message : T, val id : Long = -1L)