package cz.upce.fei.travelcards.message.response

/**
 * Response status.
 *
 * @param message describing status of operation.
 * */
data class ResponseMessage<T>(val message : T)
