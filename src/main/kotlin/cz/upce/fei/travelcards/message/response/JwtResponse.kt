package cz.upce.fei.travelcards.message.response

import org.springframework.security.core.GrantedAuthority

data class JwtResponse(
    var accessToken: String,
    var username: String,
    var authorities: MutableCollection<out GrantedAuthority>,
    var tokenType: String = "Bearer"
)