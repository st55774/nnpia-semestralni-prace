package cz.upce.fei.travelcards.message.request

data class LoginForm (
    var username: String = "",
    var password: String = ""
)