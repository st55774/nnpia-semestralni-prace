package cz.upce.fei.travelcards.message.request

import cz.upce.fei.travelcards.entity.Role

data class SignUpForm (
    var username: String= "",
    var email: String= "",
    var role: Set<Role> = mutableSetOf(),
    var password: String= "",
)