package cz.upce.fei.travelcards.message.response.tourcard

import cz.upce.fei.travelcards.entity.TourCard

data class DashBoardMessage(
    val visited: Long = 0,
    val unvisited : Long = 0,
    val tips : List<TourCard> = listOf())