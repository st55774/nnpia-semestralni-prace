package cz.upce.fei.travelcards.entity

import javax.persistence.*

@Entity
data class InterestPoint(
    @Id @GeneratedValue(strategy= GenerationType.AUTO) var id : Long = 0L,
    @Column(nullable = false, unique = true) var name : String = ""
){
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as InterestPoint

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }
}
