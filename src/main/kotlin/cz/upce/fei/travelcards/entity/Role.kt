package cz.upce.fei.travelcards.entity

import cz.upce.fei.travelcards.entity.role.RoleName
import org.hibernate.annotations.NaturalId
import javax.persistence.*

@Entity
data class Role(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id : Long = 0,
    @Enumerated(EnumType.STRING) @NaturalId @Column var roleName : RoleName = RoleName.ROLE_USER
)