package cz.upce.fei.travelcards.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
data class Photo (
    @Id @GeneratedValue(strategy= GenerationType.AUTO) var id : Long = 0L,
    @Column(nullable = false) var location : String = "",
    @OneToOne(cascade = [CascadeType.ALL], fetch = FetchType.EAGER) var author : User = User(),
)
