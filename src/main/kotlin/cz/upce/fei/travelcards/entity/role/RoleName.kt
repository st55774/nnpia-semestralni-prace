package cz.upce.fei.travelcards.entity.role

enum class RoleName {
    ROLE_USER,
    ROLE_PM,
    ROLE_ADMIN
}
