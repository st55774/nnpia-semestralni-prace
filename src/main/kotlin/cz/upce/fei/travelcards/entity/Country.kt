package cz.upce.fei.travelcards.entity

import javax.persistence.*

@Entity
data class Country (
    @Id @GeneratedValue(strategy=GenerationType.AUTO) var id : Long = 0L,
    @Column(nullable = false, unique = true) var idShort : String = "",
    @Column(nullable = false, unique = true) var name : String = "",
    @Column(nullable = false) var state : String = "",
)