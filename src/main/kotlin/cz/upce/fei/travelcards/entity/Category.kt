package cz.upce.fei.travelcards.entity

import javax.persistence.*

@Entity
data class Category(
    @Id @GeneratedValue(strategy=GenerationType.AUTO) var id : Long = 0L,
    @Column(nullable = false, unique = true) var sid : String = "",
    @Column(nullable = false, unique = true) var title : String = "",
    @ManyToOne(cascade = [CascadeType.MERGE], fetch = FetchType.LAZY) var interestPoint: InterestPoint = InterestPoint()
)