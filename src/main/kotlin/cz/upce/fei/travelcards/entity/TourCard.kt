package cz.upce.fei.travelcards.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
data class TourCard (
    @Id @GeneratedValue(strategy = GenerationType.AUTO) var id : Long = 0L,
    @Column(nullable = false, unique = true) var title : String = "",
    @Column(nullable = false, unique = true) var coordinate : String = "",
    @ManyToMany(cascade = [CascadeType.MERGE], fetch = FetchType.EAGER) @JsonIgnore var users : MutableSet<User> = mutableSetOf(),
    @OneToMany(cascade = [CascadeType.MERGE], fetch = FetchType.EAGER) var photos : MutableSet<Photo> = mutableSetOf(),
    @ManyToOne(cascade = [CascadeType.MERGE], fetch = FetchType.LAZY) var category : Category = Category(),
    @ManyToOne(cascade = [CascadeType.MERGE], fetch = FetchType.LAZY) var country : Country = Country()
){
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TourCard

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}