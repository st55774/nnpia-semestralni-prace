package cz.upce.fei.travelcards.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "users", uniqueConstraints = [
    UniqueConstraint(columnNames = ["username"]),
    UniqueConstraint(columnNames = ["email"])])
data class User(
    @Id @GeneratedValue(strategy = GenerationType.AUTO) var id : Long = 0,
    @Column(nullable = false, unique = true) var username : String = "",
    @Column(nullable = false) @JsonIgnore var password : String = "",
    @Column var create_time : Date = Date(),
    @Column(nullable = false)  var email : String = "",
    @Column(nullable = false) var active : Boolean = true,
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
        joinColumns = [JoinColumn(name = "user_id")],
        inverseJoinColumns = [JoinColumn(name = "role_id")])
    var roles : Set<Role> = mutableSetOf()
)
