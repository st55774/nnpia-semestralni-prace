package cz.upce.fei.travelcards.repository

import cz.upce.fei.travelcards.entity.Role
import cz.upce.fei.travelcards.entity.role.RoleName
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface RoleRepository : JpaRepository<Role, Long> {
    fun findByRoleName(roleName: RoleName) : Optional<Role>
    fun existsByRoleName(roleName: RoleName) : Boolean
}