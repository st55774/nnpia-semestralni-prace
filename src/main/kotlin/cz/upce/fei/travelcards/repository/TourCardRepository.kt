package cz.upce.fei.travelcards.repository

import cz.upce.fei.travelcards.entity.TourCard
import cz.upce.fei.travelcards.entity.User

import org.springframework.data.domain.Page
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.domain.Pageable

interface TourCardRepository : JpaRepository<TourCard, Long>{
    fun findAllByIdIsNot(id : Long) : List<TourCard>
    fun findAllByTitleContains(title : String, pageable: Pageable) : Page<TourCard>
    fun countTourCardByUsersIs(user : User) : Long
    fun findTourCardsByUsersIs(user: User) : List<TourCard>
    fun findTourCardsByUsersIs(user: User, pageable: Pageable) : Page<TourCard>
    fun findTourCardsByUsersIsNotIn(user: List<User>) : List<TourCard>
    fun findTourCardsByUsersIsAndTitleContains(user: User, title : String, pageable: Pageable) : Page<TourCard>
    fun existsByUsersEqualsAndIdEquals(user : User, id : Long) : Boolean
    fun existsByTitle(title : String) : Boolean
}