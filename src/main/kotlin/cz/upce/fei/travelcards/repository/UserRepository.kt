package cz.upce.fei.travelcards.repository

import cz.upce.fei.travelcards.entity.User
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface UserRepository : JpaRepository<User, Long>{
    fun findByUsername(username : String) : Optional<User>
    fun existsByUsername(username: String) : Boolean
    fun existsByEmail(email : String) : Boolean
    fun deleteByUsername(username: String)
}