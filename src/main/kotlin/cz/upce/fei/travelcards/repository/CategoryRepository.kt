package cz.upce.fei.travelcards.repository

import cz.upce.fei.travelcards.entity.Category

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository

interface CategoryRepository : JpaRepository<Category, Long>{
    fun findBySid(sid : String) : Category
    fun existsCategoryBySid(sid : String) : Boolean
    fun existsBySid(sid : String) : Boolean
    fun existsByTitle(title : String) : Boolean
    fun findAllByTitleContains(title: String, pageable: Pageable): Page<Category>
}