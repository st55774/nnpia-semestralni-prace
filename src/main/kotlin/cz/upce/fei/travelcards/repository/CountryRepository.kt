package cz.upce.fei.travelcards.repository

import cz.upce.fei.travelcards.entity.Country
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository

interface CountryRepository : JpaRepository<Country, Long>{
    fun findByIdShort(idShort : String) : Country
    fun existsCountryByIdShort(idShort: String) : Boolean
    fun findAllByNameContains(name: String, pageable: Pageable): Page<Country>
    fun existsByName(name: String): Boolean
    fun existsByIdShort(idShort: String): Boolean
}