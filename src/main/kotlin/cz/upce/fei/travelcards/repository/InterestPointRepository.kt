package cz.upce.fei.travelcards.repository

import cz.upce.fei.travelcards.entity.InterestPoint

import org.springframework.data.jpa.repository.JpaRepository

interface InterestPointRepository : JpaRepository<InterestPoint, Long> {
    fun findByName(name : String) : InterestPoint
    fun existsByName(name: String): Boolean
}