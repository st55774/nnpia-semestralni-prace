package cz.upce.fei.travelcards.repository

import cz.upce.fei.travelcards.entity.Photo
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface PhotoRepository : JpaRepository<Photo, Long>{
    fun findByLocation(location : String) : Optional<Photo>
}