package cz.upce.fei.travelcards.service.excel.upload

import org.springframework.web.multipart.MultipartFile
import java.nio.file.Path
import java.nio.file.Paths

/**
 * Import data do application database.
 * */
interface DataImport {
    /**
     * Import data from multipart file.
     *
     * @param file to import data from.
     * */
    fun import(file: MultipartFile)

    companion object {
        /**
         * Location to dave uploaded file.
         * */
        val PATH: Path = Paths.get(System.getProperty("java.io.tmpdir"))
    }

    fun check(): Boolean
}