package cz.upce.fei.travelcards.service.excel.extractor.cell

enum class CountryCell {
    SHORT, NAME, STATE
}