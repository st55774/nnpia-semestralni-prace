package cz.upce.fei.travelcards.service.security.principles

import com.fasterxml.jackson.annotation.JsonIgnore

import cz.upce.fei.travelcards.entity.User

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

import java.util.stream.Collectors

class UserPrinciple(
    private var id : Long,
    private var username : String,
    private var email : String,
    @JsonIgnore private var password : String,
    private var authorities : MutableList<GrantedAuthority>,
) : UserDetails{
    override fun getAuthorities() = authorities

    override fun getPassword() = password

    override fun getUsername() = username

    override fun isAccountNonExpired() = true

    override fun isAccountNonLocked() = true

    override fun isCredentialsNonExpired() = true

    override fun isEnabled() = true

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UserPrinciple

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    companion object{
        private val SERIAL_VERSION_UID : Long = 1L

        fun build(user : User) : UserPrinciple {
            val authorities : MutableList<GrantedAuthority> = user.roles
                .stream()
                .map{ SimpleGrantedAuthority(it.roleName.name) }
                .collect(Collectors.toList())

            return UserPrinciple(user.id, user.username, user.email, user.password, authorities)
        }
    }
}