package cz.upce.fei.travelcards.service.excel.extractor

import cz.upce.fei.travelcards.entity.Country
import cz.upce.fei.travelcards.service.excel.extractor.cell.CountryCell
import org.apache.poi.xssf.usermodel.XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.PropertySource
import org.springframework.stereotype.Service
import java.io.IOException
import java.util.*

@Service
@PropertySource("classpath:import-data.properties")
class CountryExtractor : XlsxExtractor<Country> {
    @Value("\${travelcard.country.sheet.name}")
    private val sheetName = ""

    @Value("\${travelcard.country.sheet.name.error}")
    private val sheetNameError = ""

    @Value("\${travelcard.country.sheet.header.error}")
    private val sheetHeaderError = ""

    @Value("\${travelcard.country.sheet.empty.error}")
    private val sheetEmptyError = ""

    @Value("\${travelcard.country.sheet.allowed}")
    private val sheetAllowed = ""

    private val sheetCellShort = "Zkratka"

    private val sheetCellName = "Název"

    private val sheetCellState = "Státy"

    private val minCellIndex : Short = 2

    private val minRowIndex : Short = 2

    private val headerColumns = mapOf(
        sheetCellShort to CountryCell.SHORT,
        sheetCellName to CountryCell.NAME,
        sheetCellState to CountryCell.STATE
    )

    /**
     * Read countries from Excel workbook.
     *
     * @param wb excel workbook.
     *
     * @return countries parsed from excel workbook.
     * */
    override fun readFromWorkbook(wb: XSSFWorkbook): List<Country> {
        val sheet : XSSFSheet = wb.getSheet(sheetName) ?: throw IOException(sheetNameError)
        if(sheet.lastRowNum < 2) throw IOException(sheetEmptyError)

        return readFromSheet(sheet)
    }

    /**
     * Read countries from Excel workbook sheet.
     *
     * @param sheet excel workbook sheet.
     *
     * @return countries parsed from excel workbook sheet.
     * */
    override fun readFromSheet(sheet: XSSFSheet): List<Country> {
        val allowed = sheetAllowed.split(",").toSet()

        val categories = mutableListOf<Country>()
        val header = headerCellsIndexes(sheet)

        (minRowIndex .. sheet.lastRowNum).map { sheet.getRow(it) }
            .filter { it.lastCellNum != minCellIndex }
            .filter { !Collections.disjoint(allowed, it.getCell(header[CountryCell.STATE]!!).stringCellValue.split(",")) }
            .forEach { categories.add(toCountry(it, header)) }

        return categories.toList()
    }

    /**
     * Transform excel row to country entity.
     *
     * @param row in format of country entity.
     * @param header cells representation of current row.
     *
     * @return country representation of row.
     * */
    private fun toCountry(row: XSSFRow, header: Map<CountryCell, Int>): Country {
        val country = Country()

        header.keys.forEach {
            when (it) {
                CountryCell.SHORT -> country.idShort = row.getCell(header[it]!!).stringCellValue
                CountryCell.NAME -> country.name = row.getCell(header[it]!!).stringCellValue
                CountryCell.STATE -> country.state = row.getCell(header[it]!!).stringCellValue
            }
        }

        return country
    }

    /**
     * Decode header of current sheet to cell indexes based on Category entity attributes.
     *
     * @param sheet with header to decode.
     *
     * @return cells header indexes representation.
     * */
    private fun headerCellsIndexes(sheet: XSSFSheet): Map<CountryCell, Int> {
        if(sheet.getRow(0).lastCellNum < 2) throw IOException(sheetHeaderError)

        val cells = mutableMapOf<CountryCell, Int>()
        sheet.getRow(0)
            .forEach { cell ->
                val value = cell.stringCellValue
                if(headerColumns.containsKey(value) && !cells.containsKey(headerColumns[value]))
                    cells[headerColumns[value]!!] = cell.columnIndex
                else
                    throw IOException(sheetHeaderError)
            }

        return cells
    }

}