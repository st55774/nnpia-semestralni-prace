package cz.upce.fei.travelcards.service.exception

data class CreateException(override val message : String) : Throwable()