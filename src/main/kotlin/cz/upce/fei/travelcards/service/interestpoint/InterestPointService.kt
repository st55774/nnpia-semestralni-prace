package cz.upce.fei.travelcards.service.interestpoint

import cz.upce.fei.travelcards.dto.InterestPointDto
import cz.upce.fei.travelcards.entity.InterestPoint
import cz.upce.fei.travelcards.message.response.ResponseMessage
import cz.upce.fei.travelcards.repository.InterestPointRepository
import cz.upce.fei.travelcards.service.exception.CreateException
import org.springframework.stereotype.Service

@Service
class InterestPointService(private val interestPointRepository: InterestPointRepository) {
    fun put(interestPointDto: InterestPointDto) =
        when {
            interestPointRepository.existsByName(interestPointDto.name) -> throw CreateException(POST_INTEREST_POINT_EXIST)
            interestPointDto.name.isEmpty() || interestPointDto.name.length <= MIN_LENGTH -> {
                throw CreateException(POST_NAME_FAILED)
            }
            else -> {
                interestPointRepository.save(InterestPoint(name = interestPointDto.name))
                ResponseMessage(POST_OK)
            }
        }

    private companion object{
        const val POST_OK = "Interest point has been added successfully."
        const val MIN_LENGTH = 3
        const val POST_INTEREST_POINT_EXIST = "Interest point already exist."
        const val POST_NAME_FAILED = "The name is empty or length smaller than $MIN_LENGTH."
    }
}