package cz.upce.fei.travelcards.service.excel.extractor

import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.File
import java.io.FileInputStream

/**
 * Extract data from excel workbooks.
 * 
 * @param T type parameter of extracted entity. 
 * */
interface XlsxExtractor<T> {
    /**
     * Read entities from XLSX file.
     *
     * @param path to file.
     *
     * @return entities parsed from file.
     * */
    fun readAll(path : String) : List<T> {
        val input = FileInputStream(File(path))
        val entities = readFromStream(input)

        input.close()
        return entities
    }

    /**
     * Read entities from stream Excel workbook.
     *
     * @param input excel stream.s
     *
     * @return in parsed from stream
     * */
    fun readFromStream(input: FileInputStream): List<T>{
        val wb = XSSFWorkbook(input)
        val entities = readFromWorkbook(wb)

        wb.close()
        return entities
    }

    /**
     * Read entities from Excel workbook.
     *
     * @param wb excel workbook.
     *
     * @return entities parsed from excel workbook.
     * */
    fun readFromWorkbook(wb: XSSFWorkbook): List<T>

    /**
     * Read entities from Excel workbook sheet.
     *
     * @param sheet excel workbook sheet.
     *
     * @return entities parsed from excel workbook sheet.
     * */
    fun readFromSheet(sheet: XSSFSheet): List<T>
}