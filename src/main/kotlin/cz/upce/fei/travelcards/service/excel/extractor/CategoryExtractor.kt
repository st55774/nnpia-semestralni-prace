package cz.upce.fei.travelcards.service.excel.extractor

import cz.upce.fei.travelcards.entity.Category
import cz.upce.fei.travelcards.repository.InterestPointRepository
import cz.upce.fei.travelcards.service.excel.extractor.cell.CategoryCell
import org.apache.poi.xssf.usermodel.XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.PropertySource
import org.springframework.stereotype.Service
import java.io.IOException

@Service
@PropertySource("classpath:import-data.properties")
class CategoryExtractor(private val interestPointRepository: InterestPointRepository) : XlsxExtractor<Category>{
    @Value("\${travelcard.category.sheet.name}")
    private val sheetName = ""

    @Value("\${travelcard.category.sheet.name.error}")
    private val sheetNameError = ""

    @Value("\${travelcard.category.sheet.header.error}")
    private val sheetHeaderError = ""

    @Value("\${travelcard.category.sheet.empty.error}")
    private val sheetEmptyError = ""

    private val sheetCellSid = "Zkratka"

    private val sheetCellTitle = "Název"

    private val sheetCellPointInterest = "Oblast zájmu"

    private val minCellIndex : Short = 2

    private val minRowIndex : Short = 2

    private val headerColumns = mapOf(
        sheetCellSid to CategoryCell.SID,
        sheetCellTitle to CategoryCell.TITLE,
        sheetCellPointInterest to CategoryCell.POINT_INTEREST
    )

    /**
     * Read categories from Excel workbook.
     *
     * @param wb excel workbook.
     *
     * @return categories parsed from excel workbook.
     * */
    override fun readFromWorkbook(wb: XSSFWorkbook): List<Category> {
        val sheet : XSSFSheet = wb.getSheet(sheetName) ?: throw IOException(sheetNameError)
        if(sheet.lastRowNum < 2) throw IOException(sheetEmptyError)

        return readFromSheet(sheet)
    }

    /**
     * Read categories from Excel workbook sheet.
     *
     * @param sheet excel workbook sheet.
     *
     * @return categories parsed from excel workbook sheet.
     * */
    override fun readFromSheet(sheet: XSSFSheet): List<Category> {
        val categories = mutableListOf<Category>()
        val header = headerCellsIndexes(sheet)

        (minRowIndex .. sheet.lastRowNum).map { sheet.getRow(it) }
            .filter { it.lastCellNum != minCellIndex }
            .forEach { categories.add(toCategory(it, header)) }

        return categories.toList()
    }

    /**
     * Transform excel row to category entity.
     *
     * @param row in format of category entity.
     * @param header cells representation of current row.
     *
     * @return category representation of row.
     * */
    private fun toCategory(row: XSSFRow, header: Map<CategoryCell, Int>): Category {
        val category = Category()

        header.keys.forEach {
            when (it) {
                CategoryCell.SID -> category.sid = row.getCell(header[it]!!).stringCellValue
                CategoryCell.TITLE -> category.title = row.getCell(header[it]!!).stringCellValue
                CategoryCell.POINT_INTEREST ->
                    category.interestPoint = interestPointRepository.findByName(row.getCell(header[it]!!).stringCellValue)
            }
        }

        return category
    }

    /**
     * Decode header of current sheet to cell indexes based on Category entity attributes.
     *
     * @param sheet with header to decode.
     *
     * @return cells header indexes representation.
     * */
    private fun headerCellsIndexes(sheet: XSSFSheet): Map<CategoryCell, Int> {
        if(sheet.getRow(0).lastCellNum < 2) throw IOException(sheetHeaderError)

        val cells = mutableMapOf<CategoryCell, Int>()
        sheet.getRow(0)
            .forEach { cell ->
                val value = cell.stringCellValue
                if(headerColumns.containsKey(value) && !cells.containsKey(headerColumns[value]))
                    cells[headerColumns[value]!!] = cell.columnIndex
                else
                    throw IOException(sheetHeaderError)
        }

        return cells
    }
}