package cz.upce.fei.travelcards.service.location

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.PropertySource
import org.springframework.stereotype.Service

import java.util.regex.Pattern

@Service
@PropertySource("classpath:import-data.properties")
class GpsDistanceService {
    @Value("\${travelcard.tourcard.gps.convert.exception}")
    private val illegalGpsDistanceFormat = "Illegal GPS distance format."

    fun checkGPS(degree : String) : Boolean{
        val pattern = Pattern.compile(DEGREE_REGEX)
        val matcher = pattern.matcher(degree)

        return (matcher.groupCount() == 6)
    }

    private companion object{
        private const val DEGREE_REGEX = "([0-9]+)°([0-9]+)'([0-9]+[.]*[0-9]*)\"N, ([0-9]+)°([0-9]+)'([0-9]+[.]*[0-9]*)\"E"
        private const val SECONDS = 60
        private const val MINUTES = SECONDS * SECONDS
    }
}