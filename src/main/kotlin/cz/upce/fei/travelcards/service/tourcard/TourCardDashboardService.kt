package cz.upce.fei.travelcards.service.tourcard

import cz.upce.fei.travelcards.entity.TourCard
import cz.upce.fei.travelcards.entity.User
import cz.upce.fei.travelcards.message.response.tourcard.DashBoardMessage
import cz.upce.fei.travelcards.repository.TourCardRepository
import org.springframework.stereotype.Service
import kotlin.random.Random

@Service
class TourCardDashboardService(private val tourCardRepository: TourCardRepository) {
    fun getDashboard(user: User) = DashBoardMessage(
        getTotal(user), getNotIncluded(user), getTips(user)
    )

    private fun getTotal(user : User) = tourCardRepository.countTourCardByUsersIs(user)

    private fun getNotIncluded(user: User) = tourCardRepository.count() - tourCardRepository.countTourCardByUsersIs(user)

    private fun getTips(user: User): List<TourCard> {
        val users = tourCardRepository.findTourCardsByUsersIs(user)
        val notIncluded = tourCardRepository.findAll().filterNot { users.contains(it) }

        if(notIncluded.size <= MAX) return notIncluded

        val tips = mutableListOf<TourCard>()
        (MIN until MAX).forEach { _ -> tips.add(notIncluded[Random.nextInt(MIN, notIncluded.size)])}
        return tips
    }

    private companion object{
        const val MIN = 0
        const val MAX = 3
    }
}