package cz.upce.fei.travelcards.service.tourcard

import cz.upce.fei.travelcards.dto.TourCardDto
import cz.upce.fei.travelcards.entity.TourCard
import cz.upce.fei.travelcards.message.response.record.NewRecordMessage
import cz.upce.fei.travelcards.repository.CategoryRepository
import cz.upce.fei.travelcards.repository.CountryRepository
import cz.upce.fei.travelcards.repository.TourCardRepository
import cz.upce.fei.travelcards.service.exception.CreateException
import cz.upce.fei.travelcards.service.location.GpsDistanceService

import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service

/**
 * Service for manage tour cards.
 * */
@Service
class TourCardService(
    private val tourCardRepository: TourCardRepository,
    private val categoryRepository: CategoryRepository,
    private val countryRepository: CountryRepository,
    private val gpsDistanceService: GpsDistanceService
) {
    fun find(page : Int , size : Int, search : String) =
        if(!(page < 0 || size <= 0)){
            val pageable = PageRequest.of(page, size)
            if(search.isNotEmpty() && search != DEFAULT_SEARCH)
                tourCardRepository.findAllByTitleContains(search, pageable)
            else
                tourCardRepository.findAll(pageable)
        } else {
            Page.empty()
        }

    fun putTourCard(tourCardDto: TourCardDto): NewRecordMessage<*> =
        if (tourCardDto.title.isEmpty() || tourCardDto.title.length < MIN_LENGTH)
            throw CreateException(POST_TITLE_FAILED)
        else if (!gpsDistanceService.checkGPS(tourCardDto.coordinate))
            throw CreateException(POST_COORDINATE_FAILED)
        else if (tourCardDto.country.id < 0)
            throw CreateException(POST_COUNTRY_FAILED)
        else if (tourCardDto.category.id < 0)
            throw CreateException(POST_CATEGORY_FAILED)
        else if (tourCardRepository.existsByTitle(tourCardDto.title))
            throw CreateException(POST_TOUR_CARD_EXIST)
        else {
            val country = countryRepository.findById(tourCardDto.country.id)
            val category = categoryRepository.findById(tourCardDto.category.id)

            when {
                !country.isPresent() -> throw CreateException(POST_COUNTRY_FAILED)
                !category.isPresent() -> throw CreateException(POST_CATEGORY_FAILED)
                else -> {
                    val newRecord = tourCardRepository.save(
                        TourCard(
                            title = tourCardDto.title,
                            coordinate = tourCardDto.coordinate,
                            country = country.get(),
                            category = category.get()
                        )
                    )
                    NewRecordMessage(POST_OK, newRecord.id)
                }
            }
        }

    private companion object {
        const val DEFAULT_SEARCH = "undefined"
        const val MIN_LENGTH = 3
        const val POST_OK = "The tour card has been added successfully."
        const val POST_TITLE_FAILED = "The title is empty of smaller than $MIN_LENGTH"
        const val POST_COORDINATE_FAILED = "The coordinates are invalid."
        const val POST_TOUR_CARD_EXIST = "The tour card already exist"
        const val POST_COUNTRY_FAILED = "The country not found."
        const val POST_CATEGORY_FAILED = "The category not found."
    }
}