package cz.upce.fei.travelcards.service.excel.extractor

import cz.upce.fei.travelcards.entity.InterestPoint
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.PropertySource
import org.springframework.stereotype.Service
import java.io.IOException

@Service
@PropertySource("classpath:import-data.properties")
class InterestPointExtractor : XlsxExtractor<InterestPoint>{
    @Value("\${travelcard.category.sheet.name}")
    private val sheetName = ""

    @Value("\${travelcard.category.sheet.name.error}")
    private val sheetNameError = ""

    @Value("\${travelcard.category.sheet.empty.error}")
    private val sheetEmptyError = ""

    private val sheetCellPointInterest = "Oblast zájmu"

    private val minRowIndex = 2

    private val minCellIndex : Short = 2

    /**
     * Read interest points from Excel workbook.
     *
     * @param wb excel workbook.
     *
     * @return interest points parsed from excel workbook.
     * */
    override fun readFromWorkbook(wb: XSSFWorkbook): List<InterestPoint> {
        val sheet : XSSFSheet = wb.getSheet(sheetName) ?: throw IOException(sheetNameError)
        if(sheet.lastRowNum < 2) throw IOException(sheetEmptyError)

        return readFromSheet(sheet)
    }

    /**
     * Read interest points from Excel workbook sheet.
     *
     * @param sheet excel workbook sheet.
     *
     * @return interest points parsed from excel workbook sheet.
     * */
    override fun readFromSheet(sheet: XSSFSheet): List<InterestPoint> {
        val interestPoints = mutableSetOf<InterestPoint>()
        val index = interestHeaderIndex(sheet)

        (minRowIndex .. sheet.lastRowNum).map { sheet.getRow(it) }
            .filter { it.lastCellNum != minCellIndex }
            .forEach { interestPoints.add(
                InterestPoint(name = it.getCell(index).stringCellValue)
            )}

        return interestPoints.toList()
    }

    /**
     * Return index of on InterestPoint cell.
     *
     * @param sheet with header to decode.
     *
     * @return cell index of interest point.
     * */
    private fun interestHeaderIndex(sheet: XSSFSheet) =
        sheet.getRow(0).first {
            it.stringCellValue == sheetCellPointInterest
        }.columnIndex
}