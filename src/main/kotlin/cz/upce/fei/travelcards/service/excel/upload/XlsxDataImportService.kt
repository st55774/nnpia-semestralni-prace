package cz.upce.fei.travelcards.service.excel.upload

import cz.upce.fei.travelcards.repository.CategoryRepository
import cz.upce.fei.travelcards.repository.CountryRepository
import cz.upce.fei.travelcards.repository.InterestPointRepository
import cz.upce.fei.travelcards.repository.TourCardRepository
import cz.upce.fei.travelcards.service.excel.extractor.CategoryExtractor
import cz.upce.fei.travelcards.service.excel.extractor.CountryExtractor
import cz.upce.fei.travelcards.service.excel.extractor.InterestPointExtractor
import cz.upce.fei.travelcards.service.excel.extractor.TourCardExtractor
import cz.upce.fei.travelcards.service.excel.upload.DataImport.Companion.PATH

import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

import java.nio.file.Files
import java.nio.file.Path
import java.time.LocalDateTime
import java.util.logging.Logger

@Service
class XlsxDataImportService(
    private val tourCardExtractor: TourCardExtractor,
    private val interestPointExtractor: InterestPointExtractor,
    private val countryExtractor: CountryExtractor,
    private val categoryExtractor: CategoryExtractor,

    private val tourCardRepository: TourCardRepository,
    private val interestPointRepository: InterestPointRepository,
    private val categoryRepository: CategoryRepository,
    private val countryRepository: CountryRepository) : DataImport {

    private companion object{
        val log : Logger = Logger.getLogger(XlsxDataImportService::class.java.name)
    }

    override fun import(file : MultipartFile) {
        val location = PATH.resolve("${file.originalFilename!!}-${LocalDateTime.now()}")
        Files.copy(file.inputStream, location)

        log.info("New import file has been saved at temporary dir ${location.toAbsolutePath()}")
        importEntities(location.toAbsolutePath())
    }

    override fun check() =
        tourCardRepository.count() == 0L &&
                categoryRepository.count() == 0L &&
                countryRepository.count() == 0L &&
                interestPointRepository.count() == 0L

    private fun importEntities(location : Path){
        log.info("Importing entities from ${location.toAbsolutePath()} file started")

        interestPointRepository.saveAll(interestPointExtractor.readAll(location.toString()))
        categoryRepository.saveAll(categoryExtractor.readAll(location.toString()))
        countryRepository.saveAll(countryExtractor.readAll(location.toString()))
        tourCardRepository.saveAll(tourCardExtractor.readAll(location.toString()))

        log.info("Importing entities from ${location.toAbsolutePath()} file finished")
    }
}