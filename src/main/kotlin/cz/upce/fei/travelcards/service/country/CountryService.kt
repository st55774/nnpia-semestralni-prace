package cz.upce.fei.travelcards.service.country

import cz.upce.fei.travelcards.dto.CountryDto
import cz.upce.fei.travelcards.entity.Country
import cz.upce.fei.travelcards.message.response.ResponseMessage
import cz.upce.fei.travelcards.repository.CountryRepository
import cz.upce.fei.travelcards.service.exception.CreateException
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service

/**
 * Service to manage countries
 * */
@Service
class CountryService(private val countryRepository: CountryRepository) {
    fun find(page: Int, size: Int, search: String) =
        if (!(page < 0 || size <= 0)) {
            val pageable = PageRequest.of(page, size)
            if (search.isNotEmpty() && search != DEFAULT_SEARCH)
                countryRepository.findAllByNameContains(search, pageable)
            else
                countryRepository.findAll(pageable)
        } else {
            Page.empty()
        }

    fun createCategory(countryDto: CountryDto) =
        if (countryDto.name.isEmpty() || countryDto.name.length < MIN_LENGTH)
            throw CreateException(POST_NAME_FAILED)
        else if (countryDto.idShort.isEmpty() || countryDto.idShort.length < MIN_LENGTH)
            throw CreateException(POST_ID_SHORT_FAILED)
        else if (countryDto.state.isEmpty())
            throw CreateException(POST_COUNTRY_FAILED)
        else if (countryRepository.existsByName(countryDto.name) || countryRepository.existsByIdShort(countryDto.idShort))
            throw CreateException(POST_COUNTRY_EXIST)
        else {
            countryRepository.save(Country(name = countryDto.name, idShort =  countryDto.idShort, state = countryDto.state))
            ResponseMessage(POST_OK)
        }

    fun updateCategory(countryDto: CountryDto) =
        if (!countryRepository.existsById(countryDto.id))
            throw CreateException(POST_ID_FAILED)
        else if (countryDto.name.isEmpty() || countryDto.name.length < MIN_LENGTH)
            throw CreateException(POST_NAME_FAILED)
        else if (countryDto.idShort.isEmpty() || countryDto.idShort.length < MIN_LENGTH)
            throw CreateException(POST_ID_SHORT_FAILED)
        else if (countryDto.state.isEmpty())
            throw CreateException(POST_COUNTRY_FAILED)
        else {
            val country = countryRepository.getOne(countryDto.id)
            country.idShort = countryDto.idShort
            country.name = countryDto.name
            country.state = countryDto.state

            countryRepository.saveAndFlush(country)

            ResponseMessage(POST_OK)
        }

    fun find(id: Long) = countryRepository.findById(id).get()

    private companion object {
        const val DEFAULT_SEARCH = "undefined"
        const val MIN_LENGTH = 3
        const val POST_OK = "The category has been added successfully."
        const val POST_COUNTRY_FAILED = "The country is empty"
        const val POST_ID_SHORT_FAILED = "The is short is empty or length smaller than $MIN_LENGTH."
        const val POST_NAME_FAILED = "The name is empty or length smaller than $MIN_LENGTH."
        const val POST_ID_FAILED = "The id doesnt exist."
        const val POST_COUNTRY_EXIST = "The country already exist"
    }
}