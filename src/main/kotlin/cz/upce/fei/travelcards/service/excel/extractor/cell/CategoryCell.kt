package cz.upce.fei.travelcards.service.excel.extractor.cell

enum class CategoryCell {
    SID, TITLE, POINT_INTEREST
}