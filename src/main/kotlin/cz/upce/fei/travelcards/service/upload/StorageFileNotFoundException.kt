package cz.upce.fei.travelcards.service.upload

class StorageFileNotFoundException : Throwable()