package cz.upce.fei.travelcards.service.excel.extractor

import cz.upce.fei.travelcards.entity.TourCard
import cz.upce.fei.travelcards.repository.CategoryRepository
import cz.upce.fei.travelcards.repository.CountryRepository
import cz.upce.fei.travelcards.service.excel.extractor.cell.TourCardCell
import org.apache.poi.xssf.usermodel.XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.PropertySource
import org.springframework.stereotype.Service
import java.io.IOException

@Service
@PropertySource("classpath:import-data.properties")
class TourCardExtractor(
    private val countryRepository: CountryRepository,
    private val categoryRepository: CategoryRepository) : XlsxExtractor<TourCard> {
    @Value("\${travelcard.tour.card.sheet.name}")
    private val sheetName = ""

    @Value("\${travelcard.tour.card.sheet.name.error}")
    private val sheetNameError = ""

    @Value("\${travelcard.tour.card.sheet.header.error}")
    private val sheetHeaderError = ""

    @Value("\${travelcard.tour.card.sheet.empty.error}")
    private val sheetEmptyError = ""

    private val sheetCellId = "No."

    private val sheetCellName = "Název"

    private val sheetCellCountry = "Region"

    private val sheetCellCategory = "Kategorie"

    private val sheetCellLocation = "GPS"

    private val minCellIndex : Short = 4

    private val minRowIndex : Short = 2

    private val headerColumns = mapOf(
        sheetCellId to TourCardCell.ID,
        sheetCellName to TourCardCell.NAME,
        sheetCellCountry to TourCardCell.COUNTRY,
        sheetCellCategory to TourCardCell.CATEGORY,
        sheetCellLocation to TourCardCell.LOCATION
    )

    /**
     * Read tour cards from Excel workbook.
     *
     * @param wb excel workbook.
     *
     * @return tour cards parsed from excel workbook.
     * */
    override fun readFromWorkbook(wb: XSSFWorkbook): List<TourCard> {
        val sheet : XSSFSheet = wb.getSheet(sheetName) ?: throw IOException(sheetNameError)
        if(sheet.lastRowNum < minRowIndex) throw IOException(sheetEmptyError)

        return readFromSheet(sheet)
    }

    /**
     * Read tour cards from Excel workbook sheet.
     *
     * @param sheet excel workbook sheet.
     *
     * @return tour cards parsed from excel workbook sheet.
     * */
    override fun readFromSheet(sheet: XSSFSheet): List<TourCard> {
        val categories = mutableListOf<TourCard>()
        val header = headerCellsIndexes(sheet)

        (minRowIndex .. sheet.lastRowNum).map { sheet.getRow(it) }
            .filter { it.lastCellNum != minCellIndex }
            .filter { categoryRepository.existsCategoryBySid(it.getCell(header[TourCardCell.CATEGORY]!!).stringCellValue) }
            .filter { countryRepository.existsCountryByIdShort(it.getCell(header[TourCardCell.COUNTRY]!!).stringCellValue) }
            .forEach { categories.add(toTourCard(it, header)) }

        return categories.toList()
    }

    /**
     * Transform excel row to tour card entity.
     *
     * @param row in format of tour card entity.
     * @param header cells representation of current row.
     *
     * @return tour card representation of row.
     * */
    private fun toTourCard(row: XSSFRow, header: Map<TourCardCell, Int>): TourCard {
        val tourCard = TourCard()

        header.keys.forEach {
            when (it) {
                TourCardCell.NAME -> tourCard.title = row.getCell(header[it]!!).stringCellValue
                TourCardCell.LOCATION -> tourCard.coordinate = row.getCell(header[it]!!).stringCellValue
                TourCardCell.CATEGORY -> tourCard.category = categoryRepository.findBySid(row.getCell(header[it]!!).stringCellValue)
                TourCardCell.COUNTRY -> tourCard.country = countryRepository.findByIdShort(row.getCell(header[it]!!).stringCellValue)
                else -> { /* Unused property from file */ }
            }
        }

        return tourCard
    }

    /**
     * Decode header of current sheet to cell indexes based on Tour Card entity attributes.
     *
     * @param sheet with header to decode.
     *
     * @return cells header indexes representation.
     * */
    private fun headerCellsIndexes(sheet: XSSFSheet): Map<TourCardCell, Int> {
        if(sheet.getRow(0).lastCellNum < minCellIndex) throw IOException(sheetHeaderError)

        val cells = mutableMapOf<TourCardCell, Int>()
        sheet.getRow(0)
            .forEach { cell ->
                val value = cell.stringCellValue
                if(headerColumns.containsKey(value) && !cells.containsKey(headerColumns[value]))
                    cells[headerColumns[value]!!] = cell.columnIndex
                else
                    throw IOException(sheetHeaderError)
            }

        return cells
    }
}