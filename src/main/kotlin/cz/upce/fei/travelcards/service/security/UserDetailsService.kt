package cz.upce.fei.travelcards.service.security

import cz.upce.fei.travelcards.entity.User
import cz.upce.fei.travelcards.repository.UserRepository
import cz.upce.fei.travelcards.service.security.principles.UserPrinciple

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class UserDetailsService : UserDetailsService {
    @Autowired
    var userRepository: UserRepository? = null

    @Transactional
    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails {
        val user: User = userRepository!!.findByUsername(username)
            .orElseThrow { UsernameNotFoundException("User Not Found with -> username or email : $username") }
        return UserPrinciple.build(user)
    }
}