package cz.upce.fei.travelcards.service.upload

import org.springframework.core.io.InputStreamResource
import org.springframework.core.io.Resource
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.*
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.createDirectories
import kotlin.io.path.exists

@Service
class ImageStorageService : StorageService {
    init{ init() }

    @OptIn(ExperimentalPathApi::class)
    final override fun init() {
        if(!LOCATION.exists())
            LOCATION.createDirectories()
    }

    override fun store(file: MultipartFile) : Path {
        val path = LOCATION.resolve("${generateFileName()}-${file.originalFilename}")
        Files.copy(file.inputStream, path)
        return path
    }

    override fun loadAsResource(filename: String): Resource =
        InputStreamResource(FileInputStream(File(filename)))

    companion object{
        const val PATH = "images"
        private val LOCATION : Path = Paths.get(PATH)
    }
}