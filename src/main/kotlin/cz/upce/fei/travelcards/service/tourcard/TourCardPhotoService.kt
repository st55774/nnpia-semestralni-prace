package cz.upce.fei.travelcards.service.tourcard

import cz.upce.fei.travelcards.entity.Photo
import cz.upce.fei.travelcards.entity.TourCard
import cz.upce.fei.travelcards.entity.User
import cz.upce.fei.travelcards.repository.PhotoRepository
import cz.upce.fei.travelcards.repository.TourCardRepository
import org.springframework.stereotype.Service
import java.nio.file.Path
import java.util.*
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.absolutePathString

/**
 * Service for managing images associated with tour card.
 * */
@Service
class TourCardPhotoService(
    private val photoRepository: PhotoRepository,
    private val tourCardRepository: TourCardRepository) {
    @OptIn(ExperimentalPathApi::class)
    fun addToRepositories(tourCard: TourCard, location: Path, user: User) {
        var photo = Photo(location = location.toString(), author = user)

        photo = photoRepository.save(photo)

        tourCard.photos.add(photo)
        tourCardRepository.save(tourCard)
    }
}