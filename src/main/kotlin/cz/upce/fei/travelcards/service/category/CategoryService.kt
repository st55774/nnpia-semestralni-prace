package cz.upce.fei.travelcards.service.category

import cz.upce.fei.travelcards.dto.CategoryDto
import cz.upce.fei.travelcards.entity.Category
import cz.upce.fei.travelcards.message.response.ResponseMessage
import cz.upce.fei.travelcards.repository.CategoryRepository
import cz.upce.fei.travelcards.repository.InterestPointRepository
import cz.upce.fei.travelcards.service.exception.CreateException

import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest

import org.springframework.stereotype.Service

@Service
class CategoryService(
    private val categoryRepository: CategoryRepository,
    private val interestPointRepository: InterestPointRepository) {
    fun find(page : Int , size : Int, search : String) =
        if(!(page < 0 || size <= 0)){
            val pageable = PageRequest.of(page, size)
            if(search.isNotEmpty() && search != DEFAULT_SEARCH)
                categoryRepository.findAllByTitleContains(search, pageable)
            else
                categoryRepository.findAll(pageable)
        } else {
            Page.empty()
        }

    fun crateCategory(categoryDto: CategoryDto) =
        if(categoryDto.sid.isEmpty() || categoryDto.sid.length <= MIN_LENGTH)
            throw CreateException(POST_SID_FAILED)
        else if(categoryDto.title.isEmpty() || categoryDto.title.length <= MIN_LENGTH)
            throw CreateException(POST_TITLE_FAILED)
        else if(categoryDto.interestPoint.id < 0)
            throw CreateException(POST_INTEREST_POINT_FAILED)
        else if(categoryRepository.existsBySid(categoryDto.sid) || categoryRepository.existsByTitle(categoryDto.title))
            throw CreateException(POST_CATEGORY_EXIST)
            else {
            val interestPoint = interestPointRepository.findById(categoryDto.interestPoint.id)

            if(!interestPoint.isPresent())
                throw CreateException(POST_INTEREST_POINT_FAILED)
            else {
                categoryRepository.save(
                    Category(
                    sid = categoryDto.sid,
                    title = categoryDto.title,
                    interestPoint = interestPoint.get())
                )
                ResponseMessage(POST_OK)
            }
        }

    private companion object{
        const val DEFAULT_SEARCH = "undefined"
        const val MIN_LENGTH = 3
        const val POST_OK = "The category has been added successfully."
        const val POST_SID_FAILED = "The sid is empty or length smaller than $MIN_LENGTH."
        const val POST_TITLE_FAILED = "The title is empty or length smaller than $MIN_LENGTH."
        const val POST_CATEGORY_EXIST = "The category already exist"
        const val POST_INTEREST_POINT_FAILED = "The interest point not found."
    }
}