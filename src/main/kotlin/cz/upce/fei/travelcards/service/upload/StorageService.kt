package cz.upce.fei.travelcards.service.upload

import org.springframework.core.io.Resource
import org.springframework.web.multipart.MultipartFile
import java.nio.file.Path
import java.util.*
import java.util.stream.Stream

interface StorageService {
    fun init()
    fun store(file: MultipartFile) : Path
    fun loadAsResource(filename: String): Resource
    fun generateFileName(targetStringLength : Long = 10L) : String{
        val leftLimit = 97
        val rightLimit = 122
        val random = Random()
        return random.ints(leftLimit, rightLimit + 1)
            .limit(targetStringLength)
            .collect({ StringBuilder() }, java.lang.StringBuilder::appendCodePoint, java.lang.StringBuilder::append)
            .toString()
    }
}