package cz.upce.fei.travelcards.service.tourcard

import cz.upce.fei.travelcards.dto.TourCardDto
import cz.upce.fei.travelcards.entity.TourCard
import cz.upce.fei.travelcards.entity.User
import cz.upce.fei.travelcards.repository.TourCardRepository
import cz.upce.fei.travelcards.repository.UserRepository
import cz.upce.fei.travelcards.status.EntityNotFound
import cz.upce.fei.travelcards.status.tourcard.TourCardAlreadyAdded
import cz.upce.fei.travelcards.status.tourcard.TourCardNotIncluded
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

/**
 * Managing tour cards owned by users.
 * */
@Service
class TourCardUserService(private val tourCardRepository: TourCardRepository) {
    /**
     * Find all tour cards owned by user.
     *
     * @param user to search all his tour cards.
     *
     * @return owned tour cards.
     * */
    fun all(user: User, page: Int, size: Int, search: String) =
        if (!(page < 0 || size <= 0)) {
            val pageable = PageRequest.of(page, size)
            if (search.isNotEmpty() && search != DEFAULT_SEARCH)
                tourCardRepository.findTourCardsByUsersIsAndTitleContains(user, search, pageable)
            else
                tourCardRepository.findTourCardsByUsersIs(user, pageable)
        } else {
            Page.empty()
        }

    /**
     * Add tourCard to user collection.
     *
     * @param user to check.
     * */
    fun add(tourCardDto: TourCardDto, user: User) {
        val tourCardOptional = tourCardRepository.findById(tourCardDto.id)

        if (!tourCardOptional.isPresent())
            throw EntityNotFound("Tour card with id ${tourCardDto.id} not found.")

        add(tourCardOptional.get(), user)
    }

    /**
     * Add tour card to user collection.
     *
     * @param tourCard to check.
     * @param user to check.
     * */
    private fun add(tourCard: TourCard, user: User) {
        if (!tourCardRepository.existsByUsersEqualsAndIdEquals(user, tourCard.id)) {
            tourCard.users.add(user)
            tourCardRepository.save(tourCard)
        } else {
            throw TourCardAlreadyAdded(tourCard, user)
        }
    }

    /**
     * Remove tour card from user collection.
     *
     * @param tourCardDto to remove.
     * @param user to remove.
     * */
    fun remove(tourCardDto: TourCardDto, user: User) {
        val tourCardOptional = tourCardRepository.findById(tourCardDto.id)

        if (!tourCardOptional.isPresent())
            throw EntityNotFound("Tour card with id ${tourCardDto.id} not found.")

        remove(tourCardOptional.get(), user)
    }

    /**
     * Remove tour card from user collection.
     *
     * @param tourCard to remove.
     * @param user to remove.
     * */
    private fun remove(tourCard: TourCard, user: User) {
        if (tourCardRepository.existsByUsersEqualsAndIdEquals(user, tourCard.id)) {
            tourCard.users.remove(user)
            tourCardRepository.save(tourCard)
        } else {
            throw TourCardNotIncluded(tourCard, user)
        }
    }

    /**
     * Check if tour card is in user collection.
     *
     * @param tourCardDto to check.
     * @param user to check.
     * */
    fun included(tourCardDto: TourCardDto, user: User) =
        tourCardRepository.existsByUsersEqualsAndIdEquals(user, tourCardDto.id)

    private companion object {
        const val DEFAULT_SEARCH = "undefined"
    }
}