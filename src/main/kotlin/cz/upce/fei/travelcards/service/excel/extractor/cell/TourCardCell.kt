package cz.upce.fei.travelcards.service.excel.extractor.cell

enum class TourCardCell {
    ID, NAME, COUNTRY, CATEGORY, LOCATION
}