package cz.upce.fei.travelcards.controller.auth

import cz.upce.fei.travelcards.entity.role.RoleName
import cz.upce.fei.travelcards.entity.User
import cz.upce.fei.travelcards.message.request.LoginForm
import cz.upce.fei.travelcards.message.request.SignUpForm
import cz.upce.fei.travelcards.message.response.JwtResponse
import cz.upce.fei.travelcards.repository.RoleRepository
import cz.upce.fei.travelcards.repository.UserRepository
import cz.upce.fei.travelcards.component.jwt.JwtProvider

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

@CrossOrigin(origins = ["*"], maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
class AuthenticationController(
    private val authenticationManager: AuthenticationManager,
    private val userRepository: UserRepository,
    private val roleRepository: RoleRepository,
    private val encoder : PasswordEncoder,
    private val jwtProvider: JwtProvider) {

    @PostMapping("/signin")
    fun authenticateUser(@Validated @RequestBody loginForm: LoginForm) : ResponseEntity<Any>{
        val authentication : Authentication = authenticationManager.authenticate(
            UsernamePasswordAuthenticationToken(loginForm.username, loginForm.password))

        SecurityContextHolder.getContext().authentication = authentication

        val accessToken = jwtProvider.generateJwtToken(authentication)
        val userDetail : UserDetails = authentication.principal as UserDetails

        return ResponseEntity.ok(JwtResponse(accessToken, userDetail.username, userDetail.authorities))
    }

    @PostMapping("/signup")
    fun registerUser(@Validated @RequestBody signUpRequest: SignUpForm): ResponseEntity<Any> {
        if (userRepository.existsByUsername(signUpRequest.username))
            return ResponseEntity<Any>("Fail -> Username is already taken!", HttpStatus.BAD_REQUEST)

        if (userRepository.existsByEmail(signUpRequest.email))
            return ResponseEntity<Any>("Fail -> Email is already in use!", HttpStatus.BAD_REQUEST)

        val user = User(
            username = signUpRequest.username,
            email = signUpRequest.email,
            password = encoder.encode(signUpRequest.password),
            roles = setOf(roleRepository.findByRoleName(RoleName.ROLE_USER).get())
        )

        userRepository.save(user)
        return ResponseEntity.ok().body("User registered successfully!")
    }
}