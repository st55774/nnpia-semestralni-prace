package cz.upce.fei.travelcards.controller.user

import cz.upce.fei.travelcards.entity.User
import cz.upce.fei.travelcards.repository.UserRepository
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import java.util.*

@Controller
@RequestMapping("/user")
class UserController(private val userRepository: UserRepository) {
    @GetMapping("")
    @ResponseBody
    fun all() : MutableList<User> = userRepository.findAll()

    @GetMapping("/{id}")
    @ResponseBody
    fun find(@PathVariable id : Long) : Optional<User> = userRepository.findById(id)
}