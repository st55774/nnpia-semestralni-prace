package cz.upce.fei.travelcards.controller.category

import cz.upce.fei.travelcards.dto.CategoryDto
import cz.upce.fei.travelcards.message.response.ResponseMessage
import cz.upce.fei.travelcards.service.category.CategoryService
import cz.upce.fei.travelcards.service.exception.CreateException
import org.springframework.http.ResponseEntity

import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/category")
class CategoryController(private val categoryService: CategoryService) {
    @GetMapping("")
    @ResponseBody
    fun all(@RequestParam(required = false, defaultValue = DEFAULT_PAGE.toString()) page : Int = DEFAULT_PAGE,
            @RequestParam(required = false, defaultValue = DEFAULT_SIZE.toString()) size : Int = DEFAULT_SIZE,
            @RequestParam(required = false, defaultValue = DEFAULT_SEARCH) search : String) =
        categoryService.find(page, size, search)

    @PostMapping("")
    @ResponseBody
    fun add(@RequestBody categoryDto: CategoryDto) = categoryService.crateCategory(categoryDto)

    @ExceptionHandler(CreateException::class)
    fun handleException(ex : CreateException) =
        ResponseEntity.badRequest().body(ResponseMessage(ex.message))

    private companion object{
        const val DEFAULT_PAGE = 0
        const val DEFAULT_SIZE = Int.MAX_VALUE
        const val DEFAULT_SEARCH = "undefined"
    }
}