package cz.upce.fei.travelcards.controller.tourcard

import cz.upce.fei.travelcards.dto.TourCardDto
import cz.upce.fei.travelcards.entity.TourCard
import cz.upce.fei.travelcards.message.response.ResponseMessage
import cz.upce.fei.travelcards.repository.TourCardRepository
import cz.upce.fei.travelcards.service.exception.CreateException
import cz.upce.fei.travelcards.service.tourcard.TourCardService
import org.springframework.data.domain.Page
import org.springframework.http.ResponseEntity

import org.springframework.web.bind.annotation.*

import java.util.*

@RestController
@RequestMapping("/tourcard")
class TourCardController(
    private val tourCardRepository: TourCardRepository,
    private val tourCardService: TourCardService) {

    @GetMapping("")
    @ResponseBody
    fun all(@RequestParam(required = false, defaultValue = DEFAULT_PAGE.toString()) page : Int = DEFAULT_PAGE,
            @RequestParam(required = false, defaultValue = DEFAULT_SIZE.toString()) size : Int = DEFAULT_SIZE,
            @RequestParam(required = false, defaultValue = DEFAULT_SEARCH) search : String) : Page<TourCard> =
        tourCardService.find(page, size, search)

    @GetMapping("{id}")
    @ResponseBody
    fun find(@PathVariable id : Long) : Optional<TourCard> = tourCardRepository.findById(id)

    @PostMapping("")
    @ResponseBody
    fun add(@RequestBody tourCardDto: TourCardDto) = tourCardService.putTourCard(tourCardDto)

    private companion object{
        const val DEFAULT_PAGE = 0
        const val DEFAULT_SIZE = 25
        const val DEFAULT_SEARCH = "undefined"
    }

    @ExceptionHandler(CreateException::class)
    fun handleException(ex : CreateException) =
        ResponseEntity.badRequest().body(ResponseMessage(ex.message))
}