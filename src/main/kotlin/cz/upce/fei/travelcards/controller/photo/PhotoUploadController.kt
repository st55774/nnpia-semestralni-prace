package cz.upce.fei.travelcards.controller.photo

import cz.upce.fei.travelcards.message.response.ResponseMessage
import cz.upce.fei.travelcards.repository.TourCardRepository
import cz.upce.fei.travelcards.repository.UserRepository
import cz.upce.fei.travelcards.service.upload.StorageService
import cz.upce.fei.travelcards.service.security.principles.UserPrinciple
import cz.upce.fei.travelcards.service.tourcard.TourCardPhotoService

import org.springframework.core.io.Resource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

import kotlin.io.path.ExperimentalPathApi

@Controller
@RequestMapping("/photos")
class PhotoUploadController (
    private val storageService: StorageService,
    private val userRepository: UserRepository,
    private val tourCardRepository: TourCardRepository,
    private val tourCardPhotoService: TourCardPhotoService) {
    @GetMapping("")
    @ResponseBody
    fun serverFile(@RequestParam path: String): ResponseEntity<Resource> {
        val file: Resource = storageService.loadAsResource(path)
        return ResponseEntity.ok().header(
            HttpHeaders.CONTENT_DISPOSITION,
            "attachment; filename=\"" + file.filename + "\""
        ).body(file)
    }

    @OptIn(ExperimentalPathApi::class)
    @PreAuthorize("hasRole('USER')")
    @PostMapping("")
    fun handleFileUpload(@RequestParam photo: MultipartFile, @RequestParam id: Long) : ResponseEntity<ResponseMessage<String>>{
        val location = storageService.store(photo)

        val principles = SecurityContextHolder.getContext().authentication.principal as UserPrinciple
        val tourCardOptional = tourCardRepository.getOne(id)

        tourCardPhotoService.addToRepositories(tourCardOptional, location, userRepository.findByUsername(principles.username).get())

        return ResponseEntity.status(HttpStatus.OK).body(ResponseMessage("You successfully uploaded ${photo.originalFilename}!"))
    }
}