package cz.upce.fei.travelcards.controller.interestpoint

import cz.upce.fei.travelcards.dto.InterestPointDto
import cz.upce.fei.travelcards.entity.InterestPoint
import cz.upce.fei.travelcards.message.response.ResponseMessage
import cz.upce.fei.travelcards.repository.InterestPointRepository
import cz.upce.fei.travelcards.service.exception.CreateException
import cz.upce.fei.travelcards.service.interestpoint.InterestPointService
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/interestpoint")
class InterestPointController(
    private val interestPointService: InterestPointService,
    private val interestPointRepository: InterestPointRepository
) {
    @GetMapping("")
    @ResponseBody
    fun all() : MutableList<InterestPoint> = interestPointRepository.findAll()

    @PostMapping("")
    @ResponseBody
    fun add(@Validated @RequestBody interestPointDto: InterestPointDto) = interestPointService.put(interestPointDto)

    @ExceptionHandler(CreateException::class)
    fun handleException(ex : CreateException) =
        ResponseEntity.badRequest().body(ResponseMessage(ex.message))
}