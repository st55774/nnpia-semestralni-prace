package cz.upce.fei.travelcards.controller.tourcard

import cz.upce.fei.travelcards.dto.TourCardDto
import cz.upce.fei.travelcards.entity.TourCard
import cz.upce.fei.travelcards.message.response.ResponseMessage
import cz.upce.fei.travelcards.message.response.tourcard.DashBoardMessage
import cz.upce.fei.travelcards.repository.UserRepository
import cz.upce.fei.travelcards.service.security.principles.UserPrinciple
import cz.upce.fei.travelcards.service.tourcard.TourCardDashboardService
import cz.upce.fei.travelcards.service.tourcard.TourCardUserService
import cz.upce.fei.travelcards.status.tourcard.TourCardAlreadyAdded
import cz.upce.fei.travelcards.status.tourcard.TourCardNotIncluded
import org.springframework.data.domain.Page

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/tourcarduser")
class TourCardUserController(
    private val tourCardUserService: TourCardUserService,
    private val userRepository: UserRepository,
    private val tourCardDashboardService: TourCardDashboardService
    ) {
    @GetMapping("")
    @ResponseBody
    fun owned(@RequestParam(required = false, defaultValue = DEFAULT_PAGE.toString()) page : Int = DEFAULT_PAGE,
              @RequestParam(required = false, defaultValue = DEFAULT_SIZE.toString()) size : Int = DEFAULT_SIZE,
              @RequestParam(required = false, defaultValue = DEFAULT_SEARCH) search : String): ResponseEntity<Page<TourCard>> {
        val principles = SecurityContextHolder.getContext().authentication.principal as UserPrinciple

        return ResponseEntity.status(HttpStatus.OK)
            .body(tourCardUserService.all(
                userRepository.findByUsername(principles.username).get(), page, size, search))
    }

    @GetMapping("/included/{id}")
    @ResponseBody
    fun included(@PathVariable id: Long) : ResponseEntity<ResponseMessage<Boolean>>{
        val principles = SecurityContextHolder.getContext().authentication.principal as UserPrinciple

        return ResponseEntity.status(HttpStatus.OK).body(ResponseMessage(
            tourCardUserService.included(
                TourCardDto(id),
                userRepository.findByUsername(principles.username).get())
        ))
    }

    @GetMapping("/dashboard")
    @ResponseBody
    fun dashboard() : ResponseEntity<DashBoardMessage>{
        val principles = SecurityContextHolder.getContext().authentication.principal as UserPrinciple

        return ResponseEntity.ok().body(
            tourCardDashboardService.getDashboard(userRepository.findByUsername(principles.username).get())
        )
    }

    @PostMapping("")
    @ResponseBody
    fun add(@RequestBody tourCard : TourCardDto) : ResponseEntity<ResponseMessage<String>>{
        val principles = SecurityContextHolder.getContext().authentication.principal as UserPrinciple
        if(!check(tourCard)) return ResponseEntity(HttpStatus.NOT_ACCEPTABLE)

        try {
            tourCardUserService.add(tourCard, userRepository.findByUsername(principles.username).get())
        } catch (ex : TourCardAlreadyAdded){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(ResponseMessage(ex.message!!))
        }

        return ResponseEntity(HttpStatus.OK)
    }

    @DeleteMapping("{id}")
    @ResponseBody
    fun delete(@PathVariable id: Long) : ResponseEntity<ResponseMessage<String>>{
        val principles = SecurityContextHolder.getContext().authentication.principal as UserPrinciple
        if(id < 0) return ResponseEntity(HttpStatus.NOT_ACCEPTABLE)

        try {
            tourCardUserService.remove(TourCardDto(id), userRepository.findByUsername(principles.username).get())
        } catch (ex : TourCardNotIncluded){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(ResponseMessage(ex.message!!))
        }

        return ResponseEntity(HttpStatus.OK)
    }

    private fun check(tourCardDto : TourCardDto) = tourCardDto.id >= 0

    private companion object{
        const val DEFAULT_PAGE = 0
        const val DEFAULT_SIZE = 25
        const val DEFAULT_SEARCH = "undefined"
    }
}