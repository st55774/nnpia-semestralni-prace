package cz.upce.fei.travelcards.controller.dataimport

import cz.upce.fei.travelcards.message.response.ResponseMessage
import cz.upce.fei.travelcards.service.excel.upload.DataImport

import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile

import java.util.logging.Logger

@Controller
@RequestMapping("/import")
class DataImportController(private val dataImportService: DataImport) {
    private companion object{
        val log : Logger = Logger.getLogger(DataImport::class.java.name)
    }

    @GetMapping("")
    fun check() : ResponseEntity<ResponseMessage<Boolean>> = ResponseEntity.ok().body(
        ResponseMessage(dataImportService.check()))

    @PostMapping("")
    fun importXlsx(@RequestParam("file") file : MultipartFile) : ResponseEntity<ResponseMessage<String>> =
        try {
            dataImportService.import(file)
            ResponseEntity.ok().body(ResponseMessage("Data from ${file.name} imported successfully"))
        } catch (ex : Exception){
            log.warning("Import of $file failed due to ${ex.message}")
            ResponseEntity.badRequest().body(ResponseMessage(ex.message!!))
        }
}