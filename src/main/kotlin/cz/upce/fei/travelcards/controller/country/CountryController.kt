package cz.upce.fei.travelcards.controller.country

import cz.upce.fei.travelcards.dto.CountryDto
import cz.upce.fei.travelcards.message.response.ResponseMessage
import cz.upce.fei.travelcards.service.country.CountryService
import cz.upce.fei.travelcards.service.exception.CreateException
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/country")
class CountryController(
    private val countryService : CountryService) {
    @GetMapping("")
    @ResponseBody
    fun all(@RequestParam(required = false, defaultValue = DEFAULT_PAGE.toString()) page : Int = DEFAULT_PAGE,
            @RequestParam(required = false, defaultValue = DEFAULT_SIZE.toString()) size : Int = DEFAULT_SIZE,
            @RequestParam(required = false, defaultValue = DEFAULT_SEARCH) search : String) =
        countryService.find(page, size, search)

    @GetMapping("/{id}")
    @ResponseBody
    fun id(@PathVariable id : Long) = countryService.find(id)

    @PostMapping("")
    @ResponseBody
    fun add(@RequestBody countryDto: CountryDto) = countryService.createCategory(countryDto)

    @PutMapping("")
    @ResponseBody
    fun edit(@RequestBody countryDto: CountryDto) = countryService.updateCategory(countryDto)

    @ExceptionHandler(CreateException::class)
    fun handleException(ex : CreateException) =
        ResponseEntity.badRequest().body(ResponseMessage(ex.message))

    private companion object{
        const val DEFAULT_PAGE = 0
        const val DEFAULT_SIZE = Int.MAX_VALUE
        const val DEFAULT_SEARCH = "undefined"
    }
}