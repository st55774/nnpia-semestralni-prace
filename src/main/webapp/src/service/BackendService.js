import axios from 'axios';

axios.interceptors.request.use(config => {
    const user = JSON.parse(localStorage.getItem('user'));

    if (user && user.accessToken)
        config.headers.Authorization = 'Bearer ' + user.accessToken;

    return config;
});

const SERVER_PREFIX = process.env.REACT_APP_BASE_URI

const MULTIPART_FILE_HEADERS = {"Content-Type": "multipart/form-data"}

const BackendService = {
    getUserBoard: async function () {
        return await axios.get(`${SERVER_PREFIX}/api/test/user`)
    },

    getPmBoard: async function () {
        return await axios.get(`${SERVER_PREFIX}/api/test/pm`)
    },

    getAdminBoard: async function () {
        return await axios.get(`${SERVER_PREFIX}/api/test/admin`)
    },

    postCategory: async function (data) {
        return await axios.post(`${SERVER_PREFIX}/category`, data)
    },

    getCountry : async function( id ){
        return await axios.get(`${SERVER_PREFIX}/country/${id}`)
    },

    getTourCardList: async function (page, size, search) {
        return await axios.get(`${SERVER_PREFIX}/tourcard?page=${page}&size=${size}&search=${search}`)
    },

    getTourCardDetail: async function (id) {
        return await axios.get(`${SERVER_PREFIX}/tourcard/${id}`)
    },

    getTourCardOwned: async function () {
        return await axios.get(`${SERVER_PREFIX}/tourcarduser`)
    },

    getTourCardOwnedByUsername: async function (username) {
        return await axios.get(`${SERVER_PREFIX}/tourcarduser/${username}`)
    },

    getTourCardIncluded: async function (tourCard) {
        return await axios.get(`${SERVER_PREFIX}/tourcarduser/included/${tourCard.id}`)

    },

    postAddTourCardToUser: async function (tourCard) {
        return await axios.post(`${SERVER_PREFIX}/tourcarduser`, tourCard)
    },

    deleteTourCardFromUser: async function (tourCard) {
        return await axios.delete(`${SERVER_PREFIX}/tourcarduser/${tourCard.id}`)

    },

    postTourCard: async function (value) {
        return await axios.post(`${SERVER_PREFIX}/tourcard`, value)
    },

    getInterestPointList: async function (page, size, search) {
        return await axios.get(`${SERVER_PREFIX}/interestpoint?page=${page}&size=${size}&search=${search}`)
    },

    postCountry: async function (value) {
        return await axios.post(`${SERVER_PREFIX}/country`, value)
    },

    putCountry: async function (value) {
        return await axios.put(`${SERVER_PREFIX}/country`, value)
    },

    getUserList: async function () {
        return await axios.get(`${SERVER_PREFIX}/user`)
    },

    getUserDetail: async function (id) {
        return await axios.get(`${SERVER_PREFIX}/user/${id}`)
    },

    postInterestPoint: async function (value) {
        return await axios.post(`${SERVER_PREFIX}/interestpoint`, value)
    },

    postImportData: async function (formData) {
        return await axios.post(`${SERVER_PREFIX}/import`, formData, MULTIPART_FILE_HEADERS)
    },

    postPhoto: async function (formData) {
        return await axios.post(`${SERVER_PREFIX}/photos`, formData, MULTIPART_FILE_HEADERS)
    },

    getDashBoard : async function(){
        return await axios.get(`${SERVER_PREFIX}/tourcarduser/dashboard`)
    },

    getCategoryList: async function (page, size, search) {
        let url = `${SERVER_PREFIX}/category`

        if(page !== undefined) url += `?page=${page}`
        if(size !== undefined) url += `&size=${size}`
        if(search !== undefined) url += `&search=${search}`

        return await axios.get(url)
    },

    getConsistency : async  function() {
        return await axios.get(`${SERVER_PREFIX}/import`)
    },

    getCountryList: async function (page, size, search) {
        let url = `${SERVER_PREFIX}/country`

        if(page !== undefined) url += `?page=${page}`
        if(size !== undefined) url += `&size=${size}`
        if(search !== undefined) url += `&search=${search}`

        return await axios.get(url)
    },

}

export default BackendService;