import React from 'react';
import {Route, Switch, HashRouter} from "react-router-dom";
import {Container} from "react-bootstrap";

import Home from "./component/common/Home";
import Profile from "./component/user/Profile";
import ProjectManagerPage from "./component/common/ProjectManagerPage";
import AdminPage from "./component/common/AdminPage";
import Login from "./component/auth/Login";
import Registration from "./component/auth/Registration";
import CategoryList from "./component/category/CategoryList";
import CountryList from "./component/country/CountryList";
import TourCardList from "./component/tourcard/TourCardList";
import UserList from "./component/user/UserList";
import TourCardDetail from "./component/tourcard/TourCardDetail";
import UserDetail from "./component/user/UserDetail";
import AddInterestPoint from "./component/interestpoint/AddInterestPoint";
import AddEditCountry from "./component/country/AddEditCountry";
import AddCategory from "./component/category/AddCategory";
import AddTourCard from "./component/tourcard/AddTourCard";
import AppNavbar from "./component/common/AppNavbar";
import ImportTourCard from "./component/import/ImportTourCard";

function App() {
    return (
        <div>
            <AppNavbar/>
            <Container fluid>
                <HashRouter>
                    <Switch>
                        <Route path='/' exact={true} component={Home}/>
                        <Route path='/home' exact={true} component={Home}/>
                        <Route path='/profile' exact={true} component={Profile}/>
                        <Route path='/pm' exact={true} component={ProjectManagerPage}/>
                        <Route path='/admin' exact={true} component={AdminPage}/>
                        <Route path='/signin' exact={true} component={Login}/>
                        <Route path='/signup' exact={true} component={Registration}/>
                        <Route path='/interestpoint/add' exact={true} component={AddInterestPoint}/>
                        <Route path='/country/add' exact={true} component={AddEditCountry}/>
                        <Route path='/country/edit/:id' exact={true} component={AddEditCountry}/>
                        <Route path='/category' exact={true} component={CategoryList}/>
                        <Route path='/category/add' exact={true} component={AddCategory}/>
                        <Route path='/country' exact={true} component={CountryList}/>
                        <Route path='/tourcard' exact={true} component={TourCardList}/>
                        <Route path='/tourcard/detail/:id' exact={true} component={TourCardDetail}/>
                        <Route path='/tourcard/add' exact={true} component={AddTourCard}/>
                        <Route path='/user' exact={true} component={UserList}/>
                        <Route path='/user/:id' exact={true} component={UserDetail}/>
                        <Route path='/import' exact={true} component={ImportTourCard}/>
                    </Switch>
                </HashRouter>
            </Container>
        </div>
    );
}

export default App;