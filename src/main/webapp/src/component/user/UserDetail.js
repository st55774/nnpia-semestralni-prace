import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import BackendService from "../../service/BackendService";

function UserDetail() {
    const [user, setUser] = useState({})
    const [error, setError] = useState("")
    const NO_DATA = "No data loaded"

    const {id} = useParams()

    function parseUser(response) {
        if (response !== undefined) setUser(response)
    }

    useEffect(() => {
        BackendService.getUserDetail(id)
            .then(
                response => {
                    parseUser(response.data)
                },
                error => {
                    setError(error)
                }
            )
    }, [id])

    return (
        <div>
            {error && <div>{error}</div>}
            {!user && <div>{NO_DATA}</div>}
            <h2>{user["id"]}</h2>
            <h3>{user["username"]}</h3>

            <div>
                <div>{user["email"]}</div>
                <div>{user["active"]}</div>
            </div>
        </div>
    )
}

export default UserDetail