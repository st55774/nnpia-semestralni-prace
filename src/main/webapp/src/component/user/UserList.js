import React, {useEffect, useState} from "react"
import BackendService from "../../service/BackendService"
import {Link} from "react-router-dom"

function UserList(){
    const [users, setUsers] = useState([])
    const [error, setError] = useState("")

    const NO_DATA = "No data loaded"

    function parseUsers(response) {
        if(response && response.length !== 0) setUsers(response)
    }

    useEffect(() => {
        BackendService.getUserList()
            .then(
                response => { parseUsers(response.data) },
                error => { setError(error.toString()) }
            )
    }, [])

    const properties = {
        "id" : "ID",
        "username" : "Přezdívka",
        "email" : "Email",
        "active" : "Aktivní"
    }

    return (
        <div>
            <table>
                {error && <div>{error}</div>}
                {users.length === 0 && <div>{NO_DATA}</div>}
                {users.length > 0 && (<thead>
                <tr>
                    {Object.entries(properties).map(([, value]) => <th>{value}</th>)}
                </tr>
                </thead>)}
                <tbody>
                    {users.map(data =>
                        <tr key={data.id}>
                            {Object.entries(properties).map(([key]) => <td>{data[key].toString()}</td>)}
                            <td><Link to={`/user/${data.id}`}>Detail</Link></td>
                        </tr>
                    )}
                </tbody>
            </table>
        </div>
    )
}

export default UserList