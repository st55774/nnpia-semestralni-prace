import React, {useCallback, useEffect, useState} from "react"
import BackendService from "../../service/BackendService"
import ItemList from "../list/ItemList";
import {Alert, Button, Col, Container, Row} from "react-bootstrap";
import {useHistory} from "react-router-dom";

function TourCardList(callback, deps) {
    const [tourCards, setTourCards] = useState(undefined)
    const [maxPage, setMaxPage] = useState(0)
    const [search, setSearch] = useState("")
    const [page, setPage] = useState(0)
    const [error, setError] = useState("")
    const SIZE = 5

    const history = useHistory()

    const properties = {
        "id": "Id",
        "coordinate": "GPS location",
        "title": "Title",
        "country": "Country",
        "category": "Category"
    }

    const parseTourCards = useCallback((response) => {
        if (response && response.content && response.content.length > 0) {
            toTourCards(response);
        }
        if (response.totalPages && response.totalPages > 1) {
            setMaxPage(response.totalPages)
        } else {
            setMaxPage(0)
        }
    }, [])

    useEffect(() => {
        getTourCards(page, search, (response) => {
            parseTourCards(response)
        })
    }, [maxPage, page, search, parseTourCards])

    const getTourCards = (page, search, onResponseReceived) => {
        BackendService.getTourCardList(page, SIZE, search)
            .then(
                response => {
                    onResponseReceived(response.data)
                },
                error => {
                    setError(error.toString())
                }
            )
    }

    const toTourCards = (response) => {
        let tourCards = []
        response.content.map((record) =>
            tourCards.push({
                "id": record["id"],
                "coordinate": record["coordinate"],
                "title": record["title"],
                "country": record["country"]["name"],
                "category": record["category"]["title"]
            })
        )
        setTourCards(tourCards)
    }

    return (
        <Container fluid>
            <Row><Col><h1>Tour Cards</h1></Col></Row>
            <Row>
                <Col>
                    <Button onClick={() => {history.push("/tourcard/add")}}
                        variant="outline-primary">Add Tour card</Button>
                </Col>
            </Row>
            <Row>
                <Col>
                    {error && (<Alert variant={'danger'}>{error}</Alert>)}
                </Col>
            </Row>
            <Row>
                <Col>
                <ItemList
                    items={tourCards}
                    properties={properties}
                    detailUrl={"/tourcard/detail"}
                    maxPage={maxPage}
                    button={(item) => [
                    ]}
                    onNewItemsRequest={(page, search) => {
                        getTourCards(page, search, () => {
                            setPage(page)
                            setSearch(search)
                        })
                    }}
                />
                </Col>
            </Row>
        </Container>
    )
}

export default TourCardList