import {useEffect, useState} from "react";
import BackendService from "../../service/BackendService";
import {Alert, Button, Card, Col, Container, Row} from "react-bootstrap";
import {useHistory} from 'react-router-dom'

function DashBoard() {
    const [dashBoard, setDashBoard] = useState({visited: undefined, unvisited: undefined, tips: undefined})
    const [error, setError] = useState(undefined)

    const history = useHistory()

    useEffect(() => {
        BackendService.getDashBoard()
            .then(
                (response) => {
                    response.data && setDashBoard(response.data)
                },
                (error) => {
                    setError(error.toString())
                }
            )
    }, [])

    return (
        <Container fluid>
            {error && <Alert variant={'danger'}>{error}</Alert>}
            <Row style={{marginTop: "1em"}}>
                <Col md={2} xs={6}>Unvisited places: </Col>
                <Col md={2} xs={6}>{dashBoard.unvisited} </Col>
            </Row>

            <Row style={{marginTop: "1em"}}>
                <Col md={2} xs={6}>Visited places: </Col>
                <Col md={2} xs={6}>{dashBoard.visited} </Col>
            </Row>

            {dashBoard?.tips?.length > 0 && (
                <Row style={{marginTop: "2em"}}>
                    {dashBoard.tips.map((tip) =>
                        <Col key={tip["id"]} xs={12} md={4}>
                            <Card>
                                <Card.Body>
                                    <Card.Title>{tip["title"]}</Card.Title>
                                    <Card.Text>
                                        {tip["coordinate"]}
                                    </Card.Text>
                                    <Button variant="success" onClick={() => {
                                        history.push(`/tourcard/detail/${tip["id"]}`)
                                    }}>Detail</Button>
                                </Card.Body>
                            </Card>
                        </Col>
                    )}
                </Row>
            )}
        </Container>
    )

}

export default DashBoard