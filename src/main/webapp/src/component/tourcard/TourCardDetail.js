import React, {useCallback, useEffect, useState} from "react";
import BackendService from "../../service/BackendService";
import {useParams} from "react-router-dom";
import AuthenticationService from "../../service/AuthenticationService";
import {Alert, Button, Col, Container, Image, Row} from "react-bootstrap";
import UploadFileForm from "../upload/UploadFile";
import {Map, Marker, MarkerLayer} from "react-mapycz";

function TourCardDetail() {
    const {id} = useParams()

    const [tourCard, setTourCard] = useState(undefined)
    const [coordinates, setCoordinates] = useState({})
    const [included, setIncluded] = useState(undefined)
    const [error, setError] = useState("")
    const [success, setSuccess] = useState("")

    const REGEX = "([0-9]+)°([0-9]+)'([0-9]+[.]*[0-9]*)\"N, ([0-9]+)°([0-9]+)'([0-9]+[.]*[0-9]*)\"E"

    const SERVER_PREFIX = process.env.REACT_APP_BASE_URI

    const parseTourCard = useCallback((data) => {
        if (data !== undefined) {
            setTourCard(data)
            if (AuthenticationService.isSignedIn())
                checkIncludedByUser(data)

            let found = data["coordinate"].match(REGEX)

            if(found.length === 7){
                let lat = parseInt(found[1]) + (parseInt(found[2]) / 60) + (parseInt(found[3]) / 3600)
                let long = parseInt(found[4]) + (parseInt(found[5]) / 60) + (parseInt(found[6]) / 3600)

                setCoordinates({lat: lat, lng: long})
            }
        }
    }, [])

    useEffect(() => {
        if (!tourCard && id) {
            BackendService.getTourCardDetail(id)
                .then(
                    response => {
                        parseTourCard(response.data)
                    },
                    error => {
                        setError(error.toString())
                    })
        }

    }, [id, tourCard, parseTourCard])

    const checkIncludedByUser = (data) => {
        BackendService.getTourCardIncluded(data)
            .then(
                response => {
                    setIncluded(response.data && response.data.message === true)
                },
                error => {
                    setError(error.toString())
                }
            )
    }

    const switchFromCollection = () => {
        if (included === true) {
            BackendService.deleteTourCardFromUser(tourCard)
                .then(() => {
                        setIncluded(false)
                    },
                    error => {
                        setError(error.toString())
                    })
        } else {
            BackendService.postAddTourCardToUser(tourCard)
                .then(
                    () => {
                        setIncluded(true)
                    },
                    error => {
                        setError(error.toString())
                    })
        }
    }

    const submitPhoto = (form) => {
        form.append("id", id)

        BackendService.postPhoto(form)
            .then(
                response => {
                    if (response && response.data.message)
                        setSuccess(response.data.message)
                    window.location.reload()
                },
                error => {
                    setError(error.toString())
                }
            )
    }

    return (
        <div>
            {tourCard && (<div>
                <Container fluid>
                    <Row>
                        <Col xs={8}>
                            <h2>{tourCard["title"]}</h2>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={8}>
                            <h3>{tourCard["coordinate"]}</h3>
                        </Col>
                        <Col xs={4}>
                            {AuthenticationService.isSignedIn() && (
                                <Button variant={(included) ? "secondary" : "primary"} onClick={switchFromCollection}>
                                    {included ? "Remove from collection" : "Add to collection"}
                                </Button>
                            )}
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>{tourCard["country"] && tourCard["country"]["name"]}</Col>
                    </Row>
                    <Row>
                        <Col xs={12}>{tourCard["category"] && tourCard["category"]["title"]}</Col>
                    </Row>
                </Container>
                <Container fluid>
                    <Row>
                        <Col xs={12}>
                            <Map center={coordinates}>
                                <MarkerLayer>
                                    <Marker coords={coordinates}/>
                                </MarkerLayer>
                            </Map>
                        </Col>
                    </Row>
                </Container>
                {tourCard["photos"]?.length > 0 && (
                    <Container fluid>
                        <Row>
                            <h2>Photos</h2>
                        </Row>
                        <Row>
                            {tourCard["photos"] && tourCard["photos"].length > 0 && tourCard["photos"].map((image) =>
                                <Col xs={6} md={4}>
                                    <Image key={tourCard["photos"]["id"]}
                                           src={`${SERVER_PREFIX}/${image["location"]}`} alt={tourCard["title"]}
                                           fluid/>
                                </Col>
                            )}
                        </Row>
                    </Container>
                )}
                {AuthenticationService.isSignedIn() && (
                    <Container fluid>
                        {success && (
                            <Alert variant={'success'}>{success}</Alert>
                        )}
                        {error && (
                            <Alert variant={'danger'}>{error}</Alert>
                        )}
                        <Row>
                            <Col xs={12} md={4}>
                                <h3>Upload more photohos</h3>
                                <UploadFileForm title="Upload image" paramName="photo" acceptable={".jpeg,.png"}
                                                onUploadSubmit={(form) => submitPhoto(form)}/>
                            </Col>
                        </Row>
                    </Container>
                )}
            </div>)}
        </div>
    )
}

export default TourCardDetail