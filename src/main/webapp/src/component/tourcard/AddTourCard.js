import React, {useEffect, useState} from "react";
import BackendService from "../../service/BackendService";
import {Alert, Button, Col, Container, Dropdown, Form, FormGroup, Row} from "react-bootstrap";
import {Input, Label} from "reactstrap";
import DropdownToggle from "react-bootstrap/DropdownToggle";
import DropdownMenu from "react-bootstrap/DropdownMenu";
import DropdownItem from "react-bootstrap/DropdownItem";
import {useHistory} from "react-router-dom";

function AddTourCard() {
    const [data, setData] = useState({})
    const [categoryList, setCategoryList] = useState([])
    const [countryList, setCountryList] = useState([])

    const [category, setCategory] = useState({})
    const [country, setCountry] = useState({})

    const [error, setError] = useState(undefined)
    const history = useHistory()

    const ERROR = "Check inputs."

    const validateCoordinates = (coordinate) => {
        const re = /^([0-9])+°([0-9])+'([0-9]+[.]*[0-9])*"N, ([0-9])+°([0-9])+'([0-9]+[.]*[0-9])*"E/;
        return re.test(String(coordinate));
    }

    useEffect(() => {
        BackendService.getCategoryList()
            .then(
                response => {
                    putCategories(response.data.content)
                },
                error => {
                    setError(error.toString())
                })

        BackendService.getCountryList()
            .then(response => {
                    putCountries(response.data.content)
                },
                error => {
                    setError(error.toString())
                })
    }, [])

    const putCategories = (data) => {
        if (data) {
            setCategoryList(data)
            setCategory(data[0])
        }
    }

    const putCountries = (data) => {
        if (data) {
            setCountryList(data)
            setCountry(data[0])
        }
    }

    const changeValueHandler = (name, value) => {
        setData({...data, [name]: value})
    }

    const changeCountryDropdownHandler = (id) => {
        setCountry(countryList.find((value => value.id === id)))

    }

    const changeCategoryDropdownHandler = (id) => {
        setCategory(categoryList.find((value => value.id === id)))
    }

    const addTourCardHandler = (e) => {
        e.preventDefault()

        data.category = category
        data.country = country
        BackendService.postTourCard(data)
            .then(
                response => {
                    if (response.data.id !== undefined && response.data.id >= 0) {
                        history.push(`/tourcard/detail/${response.data.id}`)
                    }
                },
                error => {
                    setError(ERROR)
                }
            )
    }

    return (
        <Container fluid>
            <Row><Col><h1>Add Tour Card</h1></Col></Row>
            {error && (<Row><Col><Alert variant="danger">{error}</Alert></Col></Row>)}
            {categoryList?.length === 0 && (<Row><Col><Alert variant="danger">No categories in database.</Alert></Col></Row>)}
            {countryList?.length === 0 && (<Row><Col><Alert variant="danger">No countries in database.</Alert></Col></Row>)}
            {categoryList?.length > 0 && countryList?.length > 0 && (
                <Row><Col><Form onSubmit={addTourCardHandler}>
                    <FormGroup style={{marginTop : "1em"}} controlId="fortitle">
                        <Label for="title">Title</Label>
                        <Input type="text" placeholder="Enter title" name="title"
                               id="title" value={data?.title || ""} autoComplete="title"
                               onChange={(e) => {
                                   changeValueHandler(e.target.name, e.target.value)
                               }}/>
                        {data?.title?.length < 3 && (<Alert variant="danger">Title at least 3 characters.</Alert>)}
                    </FormGroup>

                    <FormGroup style={{marginTop : "1em"}} controlId="forcoordinate">
                        <Label for="coordinate">Coordinates</Label>
                        <Input type="text" placeholder="Enter Coordinate" name="coordinate"
                               id="coordinate" value={data?.coordinate || ""} autoComplete="coordinate"
                               onChange={(e) => {
                                   changeValueHandler(e.target.name, e.target.value)
                               }}/>
                        {data?.coordinate && !validateCoordinates(data?.coordinate) &&
                        (<Alert variant="danger">Coordinates are not valid.</Alert>)}
                    </FormGroup>

                    <Dropdown style={{marginTop : "1em"}}>
                        {countryList?.length > 0 && (<DropdownToggle caret>{countryList[0]?.name}</DropdownToggle>)}
                        <DropdownMenu>
                            {countryList.map((value) =>
                                <DropdownItem key={value.name} onClick={() => changeCountryDropdownHandler(value.id)}
                                              dropDownValue={value.name}>
                                    {value.name}
                                </DropdownItem>
                            )}
                        </DropdownMenu>
                    </Dropdown>

                    <Dropdown style={{marginTop : "1em"}}>
                        {categoryList?.length > 0 && (<DropdownToggle caret>{categoryList[0]?.title}</DropdownToggle>)}
                        <DropdownMenu>
                            {categoryList.map((value) =>
                                <DropdownItem key={value.title} onClick={() => changeCategoryDropdownHandler(value.id)}
                                              dropDownValue={value.title}>
                                    {value.title}
                                </DropdownItem>
                            )}
                        </DropdownMenu>
                    </Dropdown>

                    <Button style={{marginTop : "1em"}} variant="primary" type="submit">Create</Button>
                </Form>
                </Col></Row>
            )}
            </Container>
    )
}

export default AddTourCard