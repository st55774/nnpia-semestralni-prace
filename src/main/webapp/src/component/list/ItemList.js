import {Input} from "reactstrap";
import {Button, Col, Row, Table} from "react-bootstrap";
import React, {useState} from "react";
import {useHistory} from "react-router-dom";
import Pages from "../page/Pages";

function ItemList({properties, items, detailUrl, maxPage, onNewItemsRequest, button}) {
    const [search, setSearch] = useState(undefined)
    const [actualPage, setActualPage] = useState(0)
    const history = useHistory()

    const onSearchInputChange = (event) => {
        if (event.target.value === "")
            setSearch(undefined)
        else
            setSearch(event.target.value)
    }

    const onSearchButtonClick = () => {
        onNewItemsRequest(actualPage, search)
    }

    const onPageChange = (page) => {
        onNewItemsRequest(page, search)
        setActualPage(page)
    }

    return (
        <div style={{"margin": "1em 0em 1em 0em"}}>
            {items?.length > 0 && (
                <div>
                    <Row>
                        <Col xs={9} md={6}>
                            <Input type="text" name="search" placeholder="Search" onChange={onSearchInputChange}/></Col>
                        <Col xs={2} md={2}>
                            <Button variant="outline-success" onClick={() => {
                                onSearchButtonClick()
                            }}>Search</Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            <Table striped bordered hover responsive style={{"margin": "1em 0em 1em 0em"}}>
                                <thead>
                                <tr>
                                    {Object.entries(properties).map(([key, value]) =>
                                        <th key={key}>{value}</th>
                                    )}
                                </tr>
                                </thead>
                                <tbody>
                                {items.map((item) =>
                                    <tr key={item.id}>
                                        {Object.entries(item).map(([key, value]) =>
                                            <td key={`${key}-${item.id}`}>{value}</td>
                                        )}
                                        {detailUrl && (<td><Button onClick={() => {
                                            history.push(`${detailUrl}/${item.id}`)
                                        }}>Detail</Button></td>)}
                                        {button && button(item).map(item => <td>{item}</td>)}
                                    </tr>
                                )}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                    {maxPage > 1 && (
                        <Pages actualPage={actualPage}
                               totalPages={maxPage}
                               onPageChangeHandler={(page) => {
                                   onPageChange(page)
                               }}/>
                    )}
                </div>
            )}
        </div>
    )
}

export default ItemList