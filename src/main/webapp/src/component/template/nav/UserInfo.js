import React from "react";
import {Nav, NavItem} from "react-bootstrap";
import {NavbarText} from "reactstrap";
import AuthenticationService from "../../../service/AuthenticationService";

function UserInfo({login, userName}) {
    const signOut = () => {
        AuthenticationService.signOut();
        window.location.reload();
    }

    return (
        <div>
            {
                login ? (
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavbarText>
                                Signed in as: <Nav.Link href="/#/profile">{userName}</Nav.Link>
                            </NavbarText>
                        </NavItem>
                        <NavItem>
                            <Nav.Link href="#" onClick={signOut}>SignOut</Nav.Link>
                        </NavItem>
                    </Nav>
                ) : (
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <Nav.Link to="/#/signin">Login</Nav.Link>
                        </NavItem>
                        <NavItem>
                            <Nav.Link to="/#/signup">SignUp</Nav.Link>
                        </NavItem>
                    </Nav>
                )
            }
        </div>
    )
}

export default UserInfo