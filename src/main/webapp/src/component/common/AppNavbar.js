import React, {useEffect, useState} from 'react'
import {Button, Nav, Navbar,} from 'react-bootstrap'
import {useHistory, withRouter} from 'react-router-dom'
import AuthenticationService from '../../service/AuthenticationService'

function AppNavbar() {
    const [user, setUser] = useState(false)

    const history = useHistory()

    useEffect(() => {
        const user = AuthenticationService.getCurrentUser();
        if (user) {setUser(true)}
    }, [])

    const signOut = () => {
        AuthenticationService.signOut()
        history.push("/#/signin")
        window.location.reload()
    }

    return (
        <div>
            <Navbar style={{paddingRight : "1em", paddingLeft : "1em"}} bg="light" expand="lg">
                <Navbar.Brand href="#home">Emča & Ondra Tourist diary</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        {user && <Nav.Link href="/#/category">Categories</Nav.Link>}
                        {user && <Nav.Link href="/#/country">Countries</Nav.Link>}
                        {user && <Nav.Link href="/#/tourcard">Tour Cards</Nav.Link>}
                        {user && AuthenticationService.isAdmin() && <Nav.Link href="/#/import">Tour card import</Nav.Link>}
                        {user && AuthenticationService.isAdmin() && <Nav.Link href="/#/signup">User registration</Nav.Link>}
                        {user && <Nav.Link><Button variant={'danger'} onClick={() => {signOut()}}>Sign out</Button></Nav.Link>}
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </div>
    )
}

export default withRouter(AppNavbar);