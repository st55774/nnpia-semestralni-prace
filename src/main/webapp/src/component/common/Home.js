import {Link} from 'react-router-dom';
import {Button, Container, Alert} from 'react-bootstrap';
import AuthenticationService from "../../service/AuthenticationService";
import DashBoard from "../tourcard/DashBoard";

function Home() {
    return (
        <div>
            <Container fluid>
                {
                    !AuthenticationService.isSignedIn() && (<div>
                        <Alert variant="primary">
                            <h2> You have to login before accessing the diary</h2>
                            <Button color="success"><Link to="/signin"><span
                                style={{color: "white"}}>Login</span></Link></Button>
                        </Alert>
                    </div>)
                }
                {
                    AuthenticationService.isSignedIn() && (
                        <div>
                            <h1 style={{marginTop: "0.5em"}}>Your experience:</h1>
                                <DashBoard />
                        </div>
                    )
                }
            </Container>
        </div>
    );
}

export default Home;