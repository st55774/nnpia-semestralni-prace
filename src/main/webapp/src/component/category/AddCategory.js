import React, {useEffect, useState} from "react";
import {Alert, Col, Container, Row} from "react-bootstrap";
import BackendService from "../../service/BackendService";
import {Button, Dropdown, Form, FormGroup} from "react-bootstrap";
import {Input, Label} from "reactstrap";
import DropdownMenu from "react-bootstrap/DropdownMenu";
import DropdownItem from "react-bootstrap/DropdownItem";
import DropdownToggle from "react-bootstrap/DropdownToggle";
import {useHistory} from "react-router-dom"

function AddCategory() {
    const [data, setData] = useState(undefined)
    const [interestPointsList, setInterestPointsList] = useState([])
    const [interestPoint, setInterestPoint] = useState({})

    const [error, setError] = useState(undefined)
    const [successful, setSuccessful] = useState(undefined)

    const history = useHistory()

    const ERROR = "Check inputs."

    useEffect(() => {
        BackendService.getInterestPointList()
            .then(
                result => {
                    putInterestPointList(result.data)
                },
                error => {
                    setError(error.toString())
                }
            )

    }, [])

    const putInterestPointList = (data) => {
        if (data !== undefined && data.length !== 0) {
            setInterestPointsList(data)
            setInterestPoint(data[0])
        }
    }

    const changeValueHandler = (name, value) => {
        setData({...data, [name]: value})
    }

    const handleChangeDropdown = (id) => {
        setInterestPoint(interestPointsList.find((value => value.id === id)))
    }

    const addCategory = (e) => {
        e.preventDefault()

        data.interestPoint = interestPoint
        BackendService.postCategory(data)
            .then(
                response => {
                    setSuccessful(response.data.message)
                    history.push("/category/")
                },
                error => {
                    setError(ERROR)
                }
            )
    }

    return (
        <Container fluid>
            <Row><Col><h1>Add Category</h1></Col></Row>
            {error && <Row><Col><Alert variant="danger">{error}</Alert></Col></Row>}
            {successful && <Row><Col><Alert variant={'success'}>successful</Alert></Col></Row>}
            {interestPointsList?.length === 0 && (<Row><Col><Alert variant="danger">No interest points in database.</Alert></Col></Row>)}
            {interestPointsList?.length > 0 && (
                <Row>
                    <Col>
                        <Form onSubmit={addCategory}>
                            <FormGroup style={{marginTop: "1em"}} controlId="forsid">
                                <Label for="sid">Id Short</Label>
                                <Input type="text" placeholder="Enter Sid" name="sid"
                                       id="sid" value={data?.sid} autoComplete="sid"
                                       onChange={(e) => {
                                           changeValueHandler(e.target.name, e.target.value)
                                       }}/>
                                {data?.sid?.length < 3 && (<Alert variant="danger">Sid at least 3 characters.</Alert>)}
                            </FormGroup>

                            <FormGroup style={{marginTop: "1em"}} controlId="fortitle">
                                <Label for="title">title</Label>
                                <Input type="text" placeholder="Enter title" name="title"
                                       id="title" value={data?.title} autoComplete="title"
                                       onChange={(e) => {
                                           changeValueHandler(e.target.name, e.target.value)
                                       }}/>
                                {data?.title?.length < 3 && (<Alert variant="danger">Title at least 3 characters.</Alert>)}
                            </FormGroup>

                            {interestPointsList?.length > 0 && (
                                <Dropdown style={{marginTop: "1em"}}>
                                    <DropdownToggle>{interestPointsList[0]?.name}</DropdownToggle>
                                    <DropdownMenu>
                                        {interestPointsList.map((value) => {
                                            return (
                                                <DropdownItem key={value.name}
                                                              onClick={() => handleChangeDropdown(value.id)}>
                                                    {value.name}
                                                </DropdownItem>
                                            )
                                        })}
                                    </DropdownMenu>
                                </Dropdown>
                            )}
                            <Button style={{marginTop: "1em"}} variant="primary" type="submit">Create</Button>
                        </Form>
                    </Col>
                </Row>
            )}
        </Container>
    )
}

export default AddCategory