import React, {useEffect, useState} from "react"
import BackendService from "../../service/BackendService"
import ItemList from "../list/ItemList";
import {Alert, Button, Col, Container, Row} from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { useCallback } from 'react'

function CategoryList(){
    const [categories, setCategories] = useState([])
    const [maxPage, setMaxPage] = useState(0)
    const [search, setSearch] = useState("")
    const [page, setPage] = useState(0)
    const [error, setError] = useState("")

    const SIZE = 15
    const history = useHistory()

    const properties = {
        "id" : "Id",
        "sid" : "Text Id",
        "title" : "Title",
        "interestPoint" : "Interest category"
    }

    const parseCategories = useCallback((response) => {
        if (response && response.content && response.content.length > 0) {
            toCategories(response);

            if(response.totalPages && response.totalPages > 1){
                setMaxPage(response.totalPages)
            } else {
                setMaxPage(0)
            }
        }
    }, [])

    useEffect(() => {
        getCategories(page, search, (response) => {
            parseCategories(response)
        })
    }, [maxPage, page, search, parseCategories])

    const getCategories = (page, search, onResponseReceived) => {
        BackendService.getCategoryList(page, SIZE, search)
            .then(
                response => {onResponseReceived(response.data)},
                error => {setError(error.toString())}
            )
    }


    const toCategories = (response) => {
        let categories = []
        for(let i = 0; i < response.content.length; ++i){
            categories.push({
                "id": response.content[i]["id"],
                "sid": response.content[i]["sid"],
                "title": response.content[i]["title"],
                "interestPoint" : response.content[i]["interestPoint"]["name"]
            })
        }
        setCategories(categories)
    }

    return (
        <Container fluid>
            <Row><Col><h1>Categories</h1></Col></Row>
            <Row>
                <Col xs={12} md={12}>
                    <Button style={{marginRight : "1em"}} onClick={() => { history.push("/category/add") }}
                            variant="outline-primary">Add Category</Button>
                    <Button onClick={() => { history.push("/interestpoint/add") }}
                            variant="outline-primary">Add Interest Point</Button>
                </Col>
            </Row>
            {error && <Row><Col><Alert variant={'danger'}>{error}</Alert></Col></Row>}
            <Row>
                <Col>
                    <ItemList
                        items={categories}
                        properties={properties}
                        detailUrl={undefined}
                        maxPage={maxPage}
                        button={(item) => [
                        ]}
                        onNewItemsRequest={(page, search) => {
                            getCategories(page, search, () => {
                                setPage(page)
                                setSearch(search)
                            })
                        }} />
                </Col>
            </Row>
        </Container>
    )
}

export default CategoryList