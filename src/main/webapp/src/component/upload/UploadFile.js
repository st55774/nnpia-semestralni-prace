import React from "react";
import { StyledDropZone } from 'react-drop-zone'
import 'react-drop-zone/dist/styles.css'
import {Button} from "react-bootstrap";

const {useState} = require("react");

function UploadFile({paramName, onUploadSubmit}){
    const [file, setFile] = useState(undefined)

    const selectFile = (file) => {
        setFile(file)
    }

    const importFile = () => {
        if(file) {
            let formData = new FormData()
            formData.append(paramName, file)
            onUploadSubmit(formData)
        }
    }

    return(
        <div>
            <StyledDropZone  onDrop={(file) => selectFile(file)} />
            <Button style={{marginTop : "1em"}} onClick={(event) => {importFile(event)}}>Upload</Button>
            {file && <div>{file.name}</div>}
        </div>
    )
}

export default UploadFile