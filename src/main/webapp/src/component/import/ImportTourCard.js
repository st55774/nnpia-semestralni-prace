import UploadFile from "../upload/UploadFile";
import BackendService from "../../service/BackendService";
import {useEffect, useState} from "react";
import {Alert, Col, Container, Row} from "react-bootstrap";

function ImportTourCard(){
    const [error, setError] = useState(undefined)
    const [success, setSuccess] = useState(undefined)
    const [consistency, setConsistency] = useState(false)

    useEffect(() => {
        BackendService.getConsistency()
            .then(response => {
                setConsistency(response?.data?.message) },
                    error => { setError(error.toString()) }
            )
    }, [])

    const onUploadHandler = (form) => {
        if(consistency){
            BackendService.postImportData(form)
                .then(
                    (response)=> { setSuccess(response?.data?.message) },
                    (error)=> { setError(error.toString()) }
                )
        }
    }

    return (
        <Container fluid>
            <Row><Col>{!consistency && <Alert variant={'warning'}>
                There is some data in database. To fresh import of tour cards. Redeploy app first and reset database
            </Alert> }</Col></Row>
            <Row style={{"margin" : "1em 0em 1em 0em"}}><Col><h1>Upload Tour cards</h1></Col></Row>
            {consistency && (
                <Row style={{"margin" : "1em 0em 1em 0em"}}>
                    <Col xs={12} md={6}>
                        <UploadFile paramName="file" onUploadSubmit={(form) => onUploadHandler(form)} />
                    </Col>
                </Row>
            )}
            <Row><Col>{error && <Alert variant={'danger'}>{error}</Alert> }</Col></Row>
            <Row><Col>{success && <Alert variant={'success'}>{success}</Alert> }</Col></Row>
        </Container>
    )
}

export default ImportTourCard