import {Alert, Button, Col, Container, Form, FormGroup, Row} from "react-bootstrap";
import {Input, Label} from "reactstrap";
import React, {useState} from "react";
import BackendService from "../../service/BackendService";
import {useHistory} from "react-router-dom"

function AddInterestPoint() {
    const [name, setName] = useState(undefined)
    const [error, setError] = useState("")

    const history = useHistory()

    const addInterestPoint = (event) => {
        event.preventDefault()

        BackendService.postInterestPoint({id: 0, name: name})
            .then(
                () => {
                    history.push("/category/")
                },
                error => {
                    setError(error.toString())
                }
            );
    }

    const changeValueHandler = (value) => {
        setName(value)
    }

    return (
        <Container fluid>
            <Row><Col><h1>Add Interest point</h1></Col></Row>
            {error && <Row><Col><Alert variant={'danger'}>error</Alert></Col></Row>}
            <Row><Col>
                <Form onSubmit={addInterestPoint}>
                    <FormGroup controlId="forname">
                        <Label for="name">Name</Label>
                        <Input type="text" placeholder="Enter your name" name="name"
                               id="name" value={name || ""} autoComplete="name"
                               onChange={(e) => {
                                   changeValueHandler(e.target.value)
                               }}/>
                        {name && name?.length < 5 && (<Alert variant="danger">Title at least 5 characters.</Alert>)}
                    </FormGroup>
                    <Button style={{marginTop: "1em"}} variant="primary" type="submit">Create</Button>
                </Form>
            </Col></Row>
        </Container>
    )
}

export default AddInterestPoint