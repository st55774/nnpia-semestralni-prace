import {Button} from "react-bootstrap";
import {useHistory} from "react-router-dom";

function AmazingButton(id){
    const history = useHistory()

    return(
        <Button onClick={(e) => {
            history.push(`/country/edit/${id}`)
        }}>
        </Button>
    )
}

export default AmazingButton