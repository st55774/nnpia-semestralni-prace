import {Alert, Button, Col, Container, Form, FormGroup, Row} from "react-bootstrap";
import {Input, Label} from "reactstrap"
import React, {useEffect, useState} from "react";
import {useHistory, useParams} from "react-router-dom"
import BackendService from "../../service/BackendService";

function AddEditCountry() {
    const [data, setData] = useState({})

    const [successful, setSuccessful] = useState(undefined)
    const [error, setError] = useState(undefined)
    const history = useHistory()
    const ERROR = "Check inputs."

    const {id} = useParams()

    useEffect(() => {
        console.log(id)
        if(id)
            BackendService.getCountry(id)
                .then(response => {
                    setData(response.data)
                })
    }, [id])

    const addCountry = (event) => {
        event.preventDefault()

        if(id){
            BackendService.putCountry(data)
                .then(
                    response => {setSuccessful(response.data.message)},
                    () => {setError(ERROR)}
                );
        } else {
            BackendService.postCountry(data)
                .then(
                    response => {
                        setSuccessful(response.data.message)
                        history.push("/country")
                    },
                    () => {setError(ERROR)}
                );
        }
    }

    const changeValueHandler = (name, value) => {
        setData({...data, [name]: value})
    }

    return (
        <Container fluid>
            <h1>Add Country</h1>
            {error && (<Row><Col><Alert variant="danger">{error}</Alert></Col></Row>)}
            {successful && <Row><Col><Alert variant={'success'}>successful</Alert></Col></Row>}
            <Row><Col>
            <Form style={{marginTop : "1em"}} onSubmit={addCountry}>
                <FormGroup style={{marginTop : "1em"}} controlId="forIdShort">
                    <Label for="idShort">Id Short</Label>
                    <Input type="text" placeholder="Enter Id short" name="idShort"
                           id="idShort" value={data?.idShort || ""} autoComplete="idShort"
                           onChange={(e) => {
                               changeValueHandler(e.target.name, e.target.value)
                           }}/>
                    {data?.idShort?.length < 3 && (<Alert variant="danger">Id Short at least 3 characters.</Alert>)}
                </FormGroup>

                <FormGroup style={{marginTop : "1em"}} controlId="forname">
                    <Label for="name">Name</Label>
                    <Input type="text" placeholder="Enter name" name="name"
                           id="name" value={data?.name || ""} autoComplete="name"
                           onChange={(e) => {
                               changeValueHandler(e.target.name, e.target.value)
                           }}/>
                    {data?.name?.length < 5 && (<Alert variant="danger">Name at least 5 characters.</Alert>)}
                </FormGroup>

                <FormGroup style={{marginTop : "1em"}} controlId="forstate">
                    <Label for="state">State</Label>
                    <Input type="text" placeholder="Enter state" name="state" value={data?.state || ""}
                           onChange={(e) => {
                               changeValueHandler(e.target.name, e.target.value)
                           }}/>
                    {data?.state < 5 && (<Alert variant="danger">State at least 5 characters.</Alert>)}
                </FormGroup>

                <Button variant="primary" type="submit">Create</Button>
            </Form>
            </Col></Row>
        </Container>
    )
}

export default AddEditCountry