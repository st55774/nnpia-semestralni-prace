import React, {useCallback, useEffect, useState} from "react"
import BackendService from "../../service/BackendService"
import {useHistory} from "react-router-dom"
import ItemList from "../list/ItemList";
import {Alert, Button, Col, Container, Row} from "react-bootstrap";

function CountryList() {
    const [countries, setCountries] = useState([])
    const [maxPage, setMaxPage] = useState(0)
    const [search, setSearch] = useState("")
    const [page, setPage] = useState(0)
    const [error, setError] = useState("")

    const SIZE = 15

    const history = useHistory()

    const properties = {
        "id": "Id",
        "idShort": "Státní identifikátor",
        "name": "Název",
        "state": "Stát"
    }

    const parseCountries = useCallback((response) => {
        if (response && response.content && response.content.length > 0) {
            setCountries(response.content)

            if (response.totalPages && response.totalPages > 1) {
                setMaxPage(response.totalPages)
            } else {
                setMaxPage(0)
            }
        }
    }, [])

    useEffect(() => {
        getCountries(maxPage, search, (response) => {
            parseCountries(response)
        })
    }, [maxPage, page, search, parseCountries])

    const getCountries = (page, search, onResponseReceived) => {
        BackendService.getCountryList(page, SIZE, search)
            .then(
                response => {
                    onResponseReceived(response.data)
                },
                error => {
                    setError(error.toString())
                }
            )
    }

    return (
        <Container fluid>
            <Row>
                <Col><h1>Countries</h1></Col>
            </Row>
            <Row>
                <Col>
                    <Button onClick={() => {
                        history.push("/country/add")
                    }} variant="outline-primary">Add County</Button>
                </Col>
            </Row>
            <Row>
                <Col>
                    {error && <Alert variant={'danger'}>{error}</Alert>}
                </Col>
            </Row>
            <Row>
                <Col>
                    <ItemList
                        items={countries}
                        properties={properties}
                        detailUrl={undefined}
                        maxPage={maxPage}
                        button={(item) => [
                            <Button variant={'warning'} onClick={(e) => {
                                history.push(`/country/edit/${item.id}`)
                            }}>EDIT</Button>,
                        ]}
                        onNewItemsRequest={(page, search) => {
                            getCountries(page, search, () => {
                                setPage(page)
                                setSearch(search)
                            })
                        }} />
                </Col>
            </Row>
        </Container>
    )
}

export default CountryList;