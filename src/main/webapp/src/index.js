import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import reportWebVitals from './reportWebVitals'
import {BrowserRouter} from "react-router-dom"
import 'bootstrap/dist/css/bootstrap.min.css'

ReactDOM.render(
  <React.Fragment>
      <BrowserRouter>
          <App />
      </BrowserRouter>
  </React.Fragment>,
  document.getElementById('root')
);

reportWebVitals();
